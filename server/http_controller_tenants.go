package server

import (
	"context"

	uuid "github.com/satori/go.uuid"
	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server/models"
)

func GetTenantSchema(sctx *ServerContext) usecase.Interactor {
	uc := usecase.NewInteractor(func(_ context.Context, _ struct{}, output *models.Schema) error {
		*output = models.TenantSchema

		return nil
	})

	uc.SetTitle("Get Tenant Form Schema")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	// uc.SetExpectedErrors(status.Internal)

	return uc
}

type options struct {
	Message string `json:"message" required:"true" description:"..."`
}

func OptionsTenantSchema(sctx *ServerContext) usecase.Interactor {
	uc := usecase.NewInteractor(func(_ context.Context, _ struct{}, output *options) error {
		*output = options{
			Message: "Options request successful",
		}
		return nil
	})

	uc.SetTitle("Options Tenant Form Schema")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	// uc.SetExpectedErrors(status.Internal)

	return uc
}

type getTenantByIDInput struct {
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func GetTenantByID(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input getTenantByIDInput, output *models.Tenant) error {
		tenantsSync.RLock()
		defer tenantsSync.RUnlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		*output = *tenant

		return nil
	})

	uc.SetTitle("Get Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type createTenantInput struct {
	ID                      string `json:"id" description:"..." required:"true"`
	Active                  bool   `json:"active" description:"..." required:"true"`
	Production              bool   `json:"production" description:"..." required:"true"`
	Code                    string `json:"code" description:"..." required:"true"`
	Name                    string `json:"name" description:"..." required:"true"`
	TaxIdentificationNumber string `json:"tax_identification_number" description:"..." required:"true"`
	PaymentsEndpoint        string `json:"payments_endpoint" description:"..." required:"true"`
	PaymentsUsername        string `json:"payments_username" description:"..." required:"true"`
	PaymentsPassword        string `json:"payments_password" description:"..." required:"true"`
	PrinterEndpoint         string `json:"printer_endpoint" description:"..." required:"true"`
	PrinterUsername         string `json:"printer_username" description:"..." required:"true"`
	PrinterPassword         string `json:"printer_password" description:"..." required:"true"`
	PrinterCode             string `json:"printer_code" description:"..." required:"true"`
	IUVAutogenerateEnabled  bool   `json:"iuv_autogenerate" required:"true"`
	IUVAux                  string `json:"iuv_aux" description:"..."`
	SegregationCode         string `json:"segregation_code" description:"..."`
	IstatCode               string `json:"istat_code" description:"..." `
	IUVPrefix               string `json:"iuv_prefix" description:"..."`
}

func CreateTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input createTenantInput, output *models.Tenant) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		_, err := tenantsCache.Get(ctx, input.ID)

		if err == nil || err.Error() != "value not found in store" {
			return status.AlreadyExists
		}

		_, err = uuid.FromString(input.ID)
		if err != nil {
			return status.InvalidArgument
		}

		output.ID = input.ID
		output.Active = input.Active
		output.Production = input.Production
		output.Code = input.Code
		output.Name = input.Name
		output.TaxIdentificationNumber = input.TaxIdentificationNumber
		output.PaymentsEndpoint = input.PaymentsEndpoint
		output.PaymentsUsername = input.PaymentsUsername
		output.PaymentsPassword = input.PaymentsPassword
		output.PrinterEndpoint = input.PrinterEndpoint
		output.PrinterUsername = input.PrinterUsername
		output.PrinterPassword = input.PrinterPassword
		output.PrinterCode = input.PrinterCode
		output.IUVAutogenerateEnabled = input.IUVAutogenerateEnabled
		if output.IUVAutogenerateEnabled {
			output.IUVAux = input.IUVAux
			output.SegregationCode = input.SegregationCode
			output.IstatCode = input.IstatCode
			output.IUVPrefix = input.IUVPrefix
		}
		err = StoreTenant(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Save Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

func OptionsCreateTenant(sctx *ServerContext) usecase.Interactor {
	uc := usecase.NewInteractor(func(_ context.Context, _ struct{}, output *options) error {
		*output = options{
			Message: "Options request successful",
		}
		return nil
	})

	uc.SetTitle("Options Save Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	// uc.SetExpectedErrors(status.Internal)

	return uc
}

type updateTenantInput struct {
	TenantId                string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ID                      string `json:"id" description:"..." required:"true"`
	Active                  bool   `json:"active" description:"..." required:"true"`
	Production              bool   `json:"production" description:"..." required:"true"`
	Code                    string `json:"code" description:"..." required:"true"`
	Name                    string `json:"name" description:"..." required:"true"`
	TaxIdentificationNumber string `json:"tax_identification_number" description:"..." required:"true"`
	PaymentsEndpoint        string `json:"payments_endpoint" description:"..." required:"true"`
	PaymentsUsername        string `json:"payments_username" description:"..." required:"true"`
	PaymentsPassword        string `json:"payments_password" description:"..." required:"true"`
	PrinterEndpoint         string `json:"printer_endpoint" description:"..." required:"true"`
	PrinterUsername         string `json:"printer_username" description:"..." required:"true"`
	PrinterPassword         string `json:"printer_password" description:"..." required:"true"`
	PrinterCode             string `json:"printer_code" description:"..." required:"true"`
	IUVAutogenerateEnabled  bool   `json:"iuv_autogenerate" required:"true"`
	IUVAux                  string `json:"iuv_aux" description:"..."`
	SegregationCode         string `json:"segregation_code" description:"..."`
	IstatCode               string `json:"istat_code" description:"..." `
	IUVPrefix               string `json:"iuv_prefix" description:"..."`
}

func UpdateTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input updateTenantInput, output *models.Tenant) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		if input.ID != tenant.ID {
			return status.InvalidArgument
		}

		output.ID = input.ID
		output.Active = input.Active
		output.Production = input.Production
		output.Code = input.Code
		output.Name = input.Name
		output.TaxIdentificationNumber = input.TaxIdentificationNumber
		output.PaymentsEndpoint = input.PaymentsEndpoint
		output.PaymentsUsername = input.PaymentsUsername
		output.PaymentsPassword = input.PaymentsPassword
		output.PrinterEndpoint = input.PrinterEndpoint
		output.PrinterUsername = input.PrinterUsername
		output.PrinterPassword = input.PrinterPassword
		output.PrinterCode = input.PrinterCode
		output.IUVAutogenerateEnabled = input.IUVAutogenerateEnabled
		if output.IUVAutogenerateEnabled {
			output.IUVAux = input.IUVAux
			output.SegregationCode = input.SegregationCode
			output.IstatCode = input.IstatCode
			output.IUVPrefix = input.IUVPrefix
		}

		err = StoreTenant(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Update Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type patchTenantInput struct {
	TenantId                string  `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Active                  *bool   `json:"active" description:"..."`
	Production              *bool   `json:"production" description:"..."`
	Code                    *string `json:"code" description:"..."`
	Name                    *string `json:"name" description:"..." required:"true"`
	TaxIdentificationNumber *string `json:"tax_identification_number" description:"..." required:"true"`
	PaymentsEndpoint        *string `json:"payments_endpoint" description:"..."`
	PaymentsUsername        *string `json:"payments_username" description:"..."`
	PaymentsPassword        *string `json:"payments_password" description:"..."`
	PrinterEndpoint         *string `json:"printer_endpoint" description:"..."`
	PrinterUsername         *string `json:"printer_username" description:"..."`
	PrinterPassword         *string `json:"printer_password" description:"..."`
	PrinterCode             *string `json:"printer_code" description:"..."`
	IUVAutogenerateEnabled  *bool   `json:"iuv_autogenerate" required:"true"`
	IUVAux                  *string `json:"iuv_aux" description:"..."`
	SegregationCode         *string `json:"segregation_code" description:"..."`
	IstatCode               *string `json:"istat_code" description:"..." `
	IUVPrefix               *string `json:"iuv_prefix" description:"..."`
}

func PatchTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input patchTenantInput, output *models.Tenant) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		output.ID = tenant.ID

		if input.Active == nil {
			output.Active = tenant.Active
		} else {
			output.Active = *input.Active
		}
		if input.Production == nil {
			output.Production = tenant.Production
		} else {
			output.Production = *input.Production
		}
		if input.Code == nil {
			output.Code = tenant.Code
		} else {
			output.Code = *input.Code
		}
		if input.Name == nil {
			output.Name = tenant.Name
		} else {
			output.Name = *input.Name
		}
		if input.TaxIdentificationNumber == nil {
			output.TaxIdentificationNumber = tenant.TaxIdentificationNumber
		} else {
			output.TaxIdentificationNumber = *input.TaxIdentificationNumber
		}
		if input.PaymentsEndpoint == nil {
			output.PaymentsEndpoint = tenant.PaymentsEndpoint
		} else {
			output.PaymentsEndpoint = *input.PaymentsEndpoint
		}
		if input.PaymentsUsername == nil {
			output.PaymentsUsername = tenant.PaymentsUsername
		} else {
			output.PaymentsUsername = *input.PaymentsUsername
		}
		if input.PaymentsPassword == nil {
			output.PaymentsPassword = tenant.PaymentsPassword
		} else {
			output.PaymentsPassword = *input.PaymentsPassword
		}
		if input.PrinterEndpoint == nil {
			output.PrinterEndpoint = tenant.PrinterEndpoint
		} else {
			output.PrinterEndpoint = *input.PrinterEndpoint
		}
		if input.PrinterUsername == nil {
			output.PrinterUsername = tenant.PrinterUsername
		} else {
			output.PrinterUsername = *input.PrinterUsername
		}
		if input.PrinterPassword == nil {
			output.PrinterPassword = tenant.PrinterPassword
		} else {
			output.PrinterPassword = *input.PrinterPassword
		}
		if input.PrinterPassword == nil {
			output.PrinterPassword = tenant.PrinterPassword
		} else {
			output.PrinterPassword = *input.PrinterPassword
		}
		if input.IUVAutogenerateEnabled == nil {
			output.IUVAutogenerateEnabled = tenant.IUVAutogenerateEnabled
		} else {
			output.IUVAutogenerateEnabled = *input.IUVAutogenerateEnabled
		}
		if input.IUVAux == nil {
			output.IUVAux = tenant.IUVAux
		} else {
			output.IUVAux = *input.IUVAux
		}
		if input.SegregationCode == nil {
			output.SegregationCode = tenant.SegregationCode
		} else {
			output.SegregationCode = *input.SegregationCode
		}
		if input.IstatCode == nil {
			output.IstatCode = tenant.IstatCode
		} else {
			output.IstatCode = *input.IstatCode
		}
		if input.IUVPrefix == nil {
			output.IUVPrefix = tenant.IUVPrefix
		} else {
			output.IUVPrefix = *input.IUVPrefix
		}

		err = StoreTenant(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Update Existing Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}
func OptionsTenantByID(sctx *ServerContext) usecase.Interactor {
	uc := usecase.NewInteractor(func(_ context.Context, input getTenantByIDInput, output *options) error {
		*output = options{
			Message: "Options request successful",
		}
		return nil
	})

	uc.SetTitle("Options Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	// uc.SetExpectedErrors(status.Internal)

	return uc
}

type disableTenantInput struct {
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func DisableTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input disableTenantInput, _ *struct{}) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		tenant.Active = false

		err = StoreTenant(sctx, tenant)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Disable Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}
