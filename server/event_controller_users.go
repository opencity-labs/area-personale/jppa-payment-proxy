package server

import (
	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
)

func HandleUserEventMessage(sctx *ServerContext, event *kafka.Message) {
	sctx.TenantsSync().RLock()
	defer sctx.TenantsSync().RUnlock()
	sctx.ServicesSync().RLock()
	defer sctx.ServicesSync().RUnlock()
	sctx.PaymentsSync().Lock()
	defer sctx.PaymentsSync().Unlock()

	FlowUserLogged := &FlowUserLogged{
		Sctx:  sctx,
		Event: event,
	}
	if !FlowUserLogged.Exec() {
		if FlowUserLogged.Err != nil {
			sctx.LogKafkaDebug().Str("flow", FlowUserLogged.Name).Str("user", FlowUserLogged.User.TaxCode).Str("result", "discarded").
				Str("flow", FlowUserLogged.Name).
				Stack().Err(FlowUserLogged.Err).
				Msg(FlowUserLogged.Msg)
		} else {
			sctx.LogKafkaDebug().Str("flow", FlowUserLogged.Name).Str("user", FlowUserLogged.User.TaxCode).Str("result", "discarded").
				Str("flow", FlowUserLogged.Name).
				Msg(FlowUserLogged.Msg)

		}
		return
	}
	flowProviderPaymentAuthenticate := &FlowProviderPaymentAuthenticate{
		Sctx:   sctx,
		Tenant: FlowUserLogged.Tenant,
	}
	if !flowProviderPaymentAuthenticate.Exec() {
		if flowProviderPaymentAuthenticate.Err != nil {
			sctx.LogKafkaDebug().Str("event", string(event.Value)).Str("result", "discarded").
				Str("flow", flowProviderPaymentAuthenticate.Name).Any("response", flowProviderPaymentAuthenticate.Result).
				Stack().Err(flowProviderPaymentAuthenticate.Err).
				Msg(flowProviderPaymentAuthenticate.Msg)
		} else {
			sctx.LogKafkaDebug().Str("event", string(event.Value)).Str("result", "discarded").
				Str("flow", flowProviderPaymentAuthenticate.Name).Any("response", flowProviderPaymentAuthenticate.Result).
				Msg(flowProviderPaymentAuthenticate.Msg)
		}
		return
	}

	FlowImportPayments := &FlowImportPayments{
		Sctx:      sctx,
		User:      FlowUserLogged.User,
		AuthToken: flowProviderPaymentAuthenticate.AuthToken,
		Tenant:    FlowUserLogged.Tenant,
	}
	if !FlowImportPayments.Exec() {
		if FlowImportPayments.Err != nil {
			sctx.LogKafkaDebug().Str("flow", FlowImportPayments.Name).Str("user", FlowImportPayments.User.TaxCode).Str("result", "discarded").
				Str("flow", FlowImportPayments.Name).
				Stack().Err(FlowImportPayments.Err).
				Msg(FlowImportPayments.Msg)
		} else {
			sctx.LogKafkaDebug().Str("flow", FlowImportPayments.Name).Str("user", FlowImportPayments.User.TaxCode).Str("result", "discarded").
				Str("flow", FlowImportPayments.Name).
				Msg(FlowImportPayments.Msg)
		}
	}

	sctx.LogKafkaInfo().Str("flow", FlowImportPayments.Name).Any("user", FlowImportPayments.User.TaxCode).Msg("user handled correctly")
}
