package server

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"io"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server/models"
)

type FlowProviderReceiptPayment struct {
	Name      string
	Sctx      *ServerContext
	Payment   *models.Payment
	Service   *models.Service
	Tenant    *models.Tenant
	AuthToken string
	Request   *http.Request
	Client    *http.Client
	Err       error
	Msg       string
	Status    bool
	Result    *models.JppaReceiptResponse
	Received  bool
	Receipt   []byte
}

func (flow *FlowProviderReceiptPayment) Exec() bool {
	flow.Name = "ProviderReceiptPayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderReceiptPayment) prepareRequest() bool {
	receiptDataRequest := models.JppaReceiptRequest{
		Printer: flow.Tenant.PrinterCode,
		Tenant:  flow.Tenant.Code,
		IUV:     flow.Payment.Payment.IUV,
	}

	receiptDataRequestJson, err := json.Marshal(receiptDataRequest)
	if err != nil {
		flow.Err = err
		flow.Msg = "json marshal error while preparing due request"
		return false
	}

	apiEndpoint := flow.Tenant.PrinterEndpoint + "/stampa-ricevuta"
	receiptReq, err := http.NewRequest("POST", apiEndpoint, bytes.NewReader(receiptDataRequestJson))
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing receipt request"
		return false
	}
	authorization := "Bearer " + flow.AuthToken
	receiptReq.Header.Add("Authorization", authorization)
	receiptReq.Header.Add("Content-Type", "application/json")

	flow.Request = receiptReq
	return true
}

func (flow *FlowProviderReceiptPayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_receipt"))

	flow.Client = &http.Client{}

	receiptRes, err := flow.Client.Do(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting receipt request"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}
	defer receiptRes.Body.Close()

	receiptBody, err := io.ReadAll(receiptRes.Body)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while reading receipt response"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	flow.Result = &models.JppaReceiptResponse{}
	err = json.Unmarshal(receiptBody, flow.Result)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error while reading receipt response"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	if flow.Result.Result == "ERROR" {
		flow.Err = err
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	receipt := flow.Result.Receipt

	if receipt == "" {
		flow.Msg = "provider response empty"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.Receipt, err = base64.StdEncoding.DecodeString(receipt)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while decoding notice"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	flow.Received = true

	return true
}
