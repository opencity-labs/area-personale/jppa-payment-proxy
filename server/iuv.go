package server

import (
	"fmt"
	"strconv"
)

func GenerateNoticeCode(sctx *ServerContext, iuvAux, pedingYear, segregationCode, IstatCode, IUVPrefix string) (string, error) {

	generatedCode, err := sctx.GenerateCode(IstatCode, pedingYear)
	if err != nil {
		return "", err
	}
	noticeCodeStr := iuvAux + segregationCode + pedingYear + IstatCode + IUVPrefix + generatedCode

	noticeCode, err := strconv.Atoi(noticeCodeStr)
	if err != nil {
		return "", err
	}
	checkDigit := noticeCode % 93
	checkDigitStr := fmt.Sprintf("%02d", checkDigit)

	return noticeCodeStr + checkDigitStr, nil
}
func GetIuvFromNoticeCode(noticeCode string) string {
	return noticeCode[1:]
}
