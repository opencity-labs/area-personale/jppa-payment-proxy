package server

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server/models"
)

type FlowImportPayments struct {
	Name                   string
	Sctx                   *ServerContext
	Tenant                 *models.Tenant
	AuthToken              string
	Request                *http.Request
	Client                 *http.Client
	Err                    error
	Msg                    string
	Status                 bool
	User                   *models.User
	ImportPaymentsResponse *models.ImportPaymentsResponse
}

func (flow *FlowImportPayments) Exec() bool {
	flow.Name = "FlowImportPayments"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest() &&
		flow.importPayments()

	return flow.Status
}

func (flow *FlowImportPayments) prepareRequest() bool {

	importPaymentsRequest := models.ImportPaymentsRequest{
		DebtorFiscalCode: flow.User.TaxCode,
		EntityCode:       flow.Tenant.Code,
	}

	importPaymentsRequestJson, err := json.Marshal(importPaymentsRequest)
	if err != nil {
		flow.Err = err
		flow.Msg = "json marshal error while preparing due request"
		return false
	}

	apiEndpoint := flow.Tenant.PaymentsEndpoint + "/estrattoConto/v1/ricerca"
	importPaymentsReq, err := http.NewRequest("POST", apiEndpoint, bytes.NewReader(importPaymentsRequestJson))
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing importPayments request"
		return false
	}
	authorization := "Bearer " + flow.AuthToken
	importPaymentsReq.Header.Add("Authorization", authorization)
	importPaymentsReq.Header.Add("Content-Type", "application/json")

	flow.Request = importPaymentsReq

	return true
}

func (flow *FlowImportPayments) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("import_payments"))

	flow.Client = &http.Client{}

	importPaymentsRes, err := flow.Client.Do(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting importPayments"

		MetricsPaymentsProviderError.WithLabelValues("import_payments").Inc()

		return false
	}
	defer importPaymentsRes.Body.Close()

	importPaymentsBody, err := io.ReadAll(importPaymentsRes.Body)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while reading importPayments response"

		MetricsPaymentsProviderError.WithLabelValues("import_payments").Inc()

		return false
	}
	if importPaymentsRes.StatusCode != http.StatusOK {
		flow.Err = errors.New(" status code: " + strconv.Itoa(importPaymentsRes.StatusCode))
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("import_payments").Inc()

		return false
	}
	flow.ImportPaymentsResponse = &models.ImportPaymentsResponse{}
	err = json.Unmarshal(importPaymentsBody, flow.ImportPaymentsResponse)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error while reading importPayments response"

		MetricsPaymentsProviderError.WithLabelValues("import_payments").Inc()

		return false
	}

	timer.ObserveDuration()
	return true
}

func (flow *FlowImportPayments) importPayments() bool {
	for _, debt := range flow.ImportPaymentsResponse.Debts {
		result, payment := flow.findPayment(debt.IDeb)
		if result == "ok" {
			resultUpdateOldPayment := flow.updateExistingPayment(&payment, debt)
			if !resultUpdateOldPayment {
				return false
			}
		}
		if result == "payment not found" {
			resultCreatePayment := flow.buildNewPayment(&payment, debt)
			if !resultCreatePayment {
				return false
			}

			if result == "ko" {
				return false
			}
		}
		resultUpdatePayment := flow.updatePayment(&payment)
		if !resultUpdatePayment {
			return false
		}
	}
	return true
}
func (flow *FlowImportPayments) findPayment(paymentId string) (string, models.Payment) {
	paymentsCache := flow.Sctx.PaymentsCache()
	result := models.Payment{}
	payment, err := paymentsCache.Get(flow.Sctx.Ctx(), paymentId)

	if err != nil && (err.Error() == "value not found in store" || err.Error() == "not found") {
		return "payment not found", result
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get payment by id error for: " + paymentId
		return "ko", result
	}
	result = *payment
	return "ok", result
}
func (flow *FlowImportPayments) updateExistingPayment(payment *models.Payment, debt models.Debt) bool {

	if payment == nil {
		flow.Err = errors.New("payment is nil pointer for debtId")
		flow.Msg = "payment is nil pointer for debtId: " + debt.IDeb
		return false
	}

	serverConfig := flow.Sctx.ServerConfig()
	now := models.TimeNow()

	if debt.Paid {
		dateTime, err := models.StringToTime(debt.PaymentDate)
		if err != nil {
			flow.Err = err
			flow.Msg = "error getting debt paid date for debtID: " + debt.IDeb
			return false
		}
		payment.Status = "COMPLETE"

		payment.Payment.PaidAt = dateTime
	}
	dateTime, err := models.StringToTime(debt.DueDate)
	if err != nil {
		flow.Err = err
		flow.Msg = "error getting DueDate for debtID: " + debt.IDeb
		return false
	}
	if payment.ServiceID == "" {
		serviceId, err := GetServiceIdDebtType(flow.Sctx, debt.DebtTypeCode, payment.TenantID)
		if err != nil {
			flow.Err = err
			flow.Msg = "error retrieving service id"
			return false
		}
		if serviceId == "" {
			serviceId = flow.User.ID
		}
		payment.ServiceID = serviceId

	}
	if payment.RemoteID == "" {
		payment.RemoteID = payment.ServiceID
	}

	payment.Payment.ExpireAt = dateTime
	payment.Links.OnlinePaymentBegin.Url = serverConfig.ExternalApiUrl + serverConfig.BasePath + "online-payment/" + payment.ID
	payment.Links.OnlinePaymentBegin.LastOpenedAt = models.Time{}
	payment.Links.OnlinePaymentBegin.Method = "GET"

	payment.Links.OfflinePayment.Url = serverConfig.ExternalApiUrl + serverConfig.BasePath + "notice/" + payment.ID
	payment.Links.OfflinePayment.LastOpenedAt = models.Time{}
	payment.Links.OfflinePayment.Method = "GET"

	payment.Links.Receipt.Url = serverConfig.ExternalApiUrl + serverConfig.BasePath + "receipt/" + payment.ID
	payment.Links.Receipt.LastOpenedAt = models.Time{}
	payment.Links.Receipt.Method = "GET"

	payment.Links.Update.Url = serverConfig.InternalApiUrl + serverConfig.BasePath + "update/" + payment.ID
	payment.Links.Update.LastCheckAt = now
	payment.Links.Update.NextCheckAt = payment.NextCheck()
	payment.Links.Update.Method = "GET"

	payment.Payer.StreetName = flow.User.Residency.Address
	payment.Payer.PostalCode = flow.User.Residency.PostCode
	payment.Payer.TownName = flow.User.Residency.Municipality
	payment.Payer.CountrySubdivision = flow.User.Residency.County
	payment.Payer.Email = flow.User.Email

	payment.UpdatedAt = now
	payment.EventCreatedAt = now
	payment.EventID = uuid.NewV4().String()
	payment.AppID = serverConfig.AppName + ":" + VERSION
	return true
}

func (flow *FlowImportPayments) buildNewPayment(payment *models.Payment, debt models.Debt) bool {

	if payment == nil {
		flow.Err = errors.New("payment is nil pointer for debtId")
		flow.Msg = "payment is nil pointer for debtId: " + debt.IDeb
		return false
	}

	serverConfig := flow.Sctx.ServerConfig()
	now := models.TimeNow()
	payment.ID = debt.IDeb
	payment.UserID = flow.User.ID
	payment.TenantID = flow.User.TenantID

	serviceId, err := GetServiceIdDebtType(flow.Sctx, debt.DebtTypeCode, payment.TenantID)
	if err != nil {
		flow.Err = err
		flow.Msg = "error retrieving service id"
		return false
	}
	if serviceId == "" {
		serviceId = flow.User.ID
	}
	payment.ServiceID = serviceId
	payment.RemoteID = serviceId
	payment.Type = "PAGOPA"
	fmt.Println("112233 FlowImportPayments debt.Paid: " + strconv.FormatBool(debt.Paid))
	if !debt.Paid {
		payment.Status = "PAYMENT_PENDING"
	} else {
		payment.Status = "COMPLETE"
		dateTime, err := models.StringToTime(debt.PaymentDate)
		if err != nil {
			flow.Err = err
			flow.Msg = "error getting debt paid date for debtID: " + debt.IDeb
			return false
		}
		payment.Payment.PaidAt = dateTime
	}
	dateTime, err := models.StringToTime(debt.DueDate)
	if err != nil {
		flow.Err = err
		flow.Msg = "error getting DueDate for debtID: " + debt.IDeb
		return false
	}
	payment.Payment.ExpireAt = dateTime

	payment.Reason = debt.PaymentReason
	payment.Payment.Amount = debt.DebtAmount
	payment.Payment.Currency = "EUR"
	payment.Payment.NoticeCode = debt.NoticeNumber
	payment.Payment.IUD = debt.UniquePaymentIdentifier
	payment.Payment.IUV = debt.UniquePaymentIdentifier

	onlinePaymentLanding, err := flow.buildOnlinePaymentLandingUrl()
	if err != nil {
		flow.Err = err
		flow.Msg = "error building Online Payment Landing Url "
		return false
	}
	payment.Links.OnlinePaymentLanding.Url = onlinePaymentLanding
	//payment.Links.OfflinePayment.LastOpenedAt = models.Time{}
	payment.Links.OnlinePaymentLanding.Method = "GET"

	payment.Links.OnlinePaymentBegin.Url = serverConfig.ExternalApiUrl + serverConfig.BasePath + "online-payment/" + payment.ID
	payment.Links.OnlinePaymentBegin.LastOpenedAt = models.Time{}
	payment.Links.OnlinePaymentBegin.Method = "GET"

	payment.Links.OfflinePayment.Url = serverConfig.ExternalApiUrl + serverConfig.BasePath + "notice/" + payment.ID
	payment.Links.OfflinePayment.LastOpenedAt = models.Time{}
	payment.Links.OfflinePayment.Method = "GET"

	payment.Links.Receipt.Url = serverConfig.ExternalApiUrl + serverConfig.BasePath + "receipt/" + payment.ID
	payment.Links.Receipt.LastOpenedAt = models.Time{}
	payment.Links.Receipt.Method = "GET"

	payment.Links.Update.Url = serverConfig.InternalApiUrl + serverConfig.BasePath + "update/" + payment.ID
	payment.Links.Update.LastCheckAt = now
	payment.Links.Update.NextCheckAt = payment.NextCheck()
	payment.Links.Update.Method = "GET"

	var paymentNotification []models.PaymentNotification
	paymentNotification = append(paymentNotification, models.PaymentNotification{})
	payment.Links.Notify = paymentNotification
	payment.Links.Notify[0].Url = "#"

	payment.Payer.Type = "human"
	payment.Payer.TaxIdentificationNumber = flow.User.TaxCode
	payment.Payer.Name = flow.User.GivenName
	payment.Payer.StreetName = flow.User.Residency.Address
	payment.Payer.PostalCode = flow.User.Residency.PostCode
	payment.Payer.TownName = flow.User.Residency.Municipality
	payment.Payer.CountrySubdivision = flow.User.Residency.County
	payment.Payer.Email = flow.User.Email

	payment.UpdatedAt = now
	payment.EventCreatedAt = now
	payment.EventID = uuid.NewV4().String()
	payment.EventVersion = "2.0"
	payment.CreatedAt = now
	payment.AppID = serverConfig.AppName + ":" + VERSION
	return true
}

func (flow *FlowImportPayments) updatePayment(payment *models.Payment) bool {
	err := StorePayment(flow.Sctx, payment)
	if err != nil && err.Error() == "invalid data" {
		flow.Err = err
		flow.Msg = "json marshal error"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "storage error"
		return false
	}
	err = ProducePayment(flow.Sctx, payment)
	if err != nil && err.Error() == "invalid data" {
		flow.Err = err
		flow.Msg = "json marshal error"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "kafka producer error"
		return false
	}
	return true
}

func (flow *FlowImportPayments) buildOnlinePaymentLandingUrl() (string, error) {
	pbClient := flow.Sctx.PBClient
	err := pbClient.getToken()
	if err != nil {
		return "", err
	}
	url, err := pbClient.GetTenantURL(flow.User.TenantID)
	if err != nil {
		return "", err
	}
	return url + "/it/user/payments", nil

}
