package models

type JppaOnlineRequest struct {
	TenantCode  string                    `json:"codIpaRichiedente"`    // tenant.Code
	ServiceCode string                    `json:"codiceServizio"`       // service.Code
	Debits      []*JppaOnlineRequestDebit `json:"debiti"`               // json string
	PaymentID   string                    `json:"idRichiestaPagamento"` // payment.payment.iud
	Payers      bool                      `json:"multibeneficiario"`    // sempre false
	Payer       *JppaOnlineRequestPayer   `json:"versante"`
	Links       *JppaOnlineRequestLinks   `json:"backUrlDto"`
}

type JppaOnlineRequestDebit struct {
	ReasonUpdate    string                    `json:"causaAggStato"`        // sempre ALTRO
	Reason          string                    `json:"causaleDebito"`        // service.kind
	TenantCode      string                    `json:"codIpaCreditore"`      // tenant.code
	RequesterCode   string                    `json:"codIpaRichiedente"`    // tenant.code
	OnlineID        string                    `json:"codiceLotto"`          // payment.iud
	ServiceCode     string                    `json:"codiceServizio"`       // service.code
	OnlineKind      string                    `json:"codiceTipoDebito"`     // service.kind
	ActivedAt       string                    `json:"dataAttualizzazione"`  // YYYY-MM-DDT00:00:00.000Z
	ExpireAt        string                    `json:"dataFineValidita"`     // YYYY-MM-DDT00:00:00.000Z
	CreatedAt       string                    `json:"dataInizioValidita"`   // YYYY-MM-DDT00:00:00.000Z
	PaymentExpireAt string                    `json:"dataLimitePagabilita"` // YYYY-MM-DDT00:00:00.000Z
	Splits          []*JppaOnlineRequestSplit `json:"datiAccertamento"`
	Group           string                    `json:"gruppo"`     // 1
	PaymentID       string                    `json:"idDebitoBO"` // payment.iud
	Amount          float64                   `json:"importoDebito"`
	Order           int32                     `json:"ordinamento"` // 1
	Payer           *JppaOnlineRequestPayer   `json:"contribuenteDto"`
	Description     string                    `json:"dettaglioPosizione"` // service.descriptiom
	PositionID      string                    `json:"idPosizioneBO"`      // payment.iud
}

type JppaOnlineRequestSplit struct {
	Year             int32   `json:"annoAccertamento"`        // anno di riferimento del pagamento
	EntryCode        string  `json:"codiceAccertamento"`      // service.entry_code ||  service.split.entry_code
	EntryDescription string  `json:"descrizioneAccertamento"` // service.entry_description ||  service.split.entry_description
	Amount           float64 `json:"importoAccertamento"`
}

type JppaOnlineRequestPayer struct {
	PayerType                    string `json:"tipoIdentificativoUnivoco"` // "F" fisica o "G" giuridica
	PayerTaxIdentificationNumber string `json:"codiceIdentificativoUnivoco,omitempty"`
	PayerCompanyName             string `json:"ragioneSociale,omitempty"`
	PayerName                    string `json:"nome,omitempty"`
	PayerFamilyName              string `json:"cognome,omitempty"`
	PayerEmail                   string `json:"email,omitempty"`
	PayerStreetName              string `json:"indirizzo,omitempty"`
	PayerBuildingNumber          string `json:"civico,omitempty"`
	PayerPostalCode              int32  `json:"cap,omitempty"`
	PayerTownName                string `json:"localita,omitempty"`
	PayerCountrySubdivision      string `json:"provincia,omitempty"`
	PayerCountry                 string `json:"nazione,omitempty"`
}

type JppaOnlineRequestLinks struct {
	ReturnCancel string `json:"backUrlCancel"`
	ReturnKO     string `json:"backUrlKO"`
	ReturnNotify string `json:"backUrlNotify"`
	ReturnOK     string `json:"backUrlOK"`
}
type JppaOnlineResponse struct {
	// ErrorCode     string                    `json:"errorCode"` // se presente chiamata ko
	TenantCode    *string                   `json:"codIpaRichiedente"`
	ServiceCode   *string                   `json:"codiceServizio"`
	Result        *JppaOnlineResponseResult `json:"esitoDto"`
	UrlRedirect   string                    `json:"url"`              // se presente chiamata ok
	TransactionID string                    `json:"identTransazione"` // se presente chiamata ok
}

type JppaOnlineResponseResult struct {
	Result string `json:"esito"`
}
