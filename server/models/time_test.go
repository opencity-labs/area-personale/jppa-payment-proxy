package models

import (
	"testing"
	"time"
)

func TestStringToTime(t *testing.T) {
	tests := []struct {
		input    string
		expected time.Time
		wantErr  bool
	}{
		{
			input:    "2024-09-24T22:00:00Z",
			expected: time.Date(2024, 9, 24, 22, 0, 0, 0, time.UTC),
			wantErr:  false,
		},
		{
			input:    "2024-09-24T22:00:00.000Z",
			expected: time.Date(2024, 9, 24, 22, 0, 0, 0, time.UTC),
			wantErr:  false,
		},
		{
			input:    "invalid-date",
			expected: time.Time{},
			wantErr:  true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.input, func(t *testing.T) {
			result, err := StringToTime(tt.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("StringToTime() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && !result.Time.Equal(tt.expected) {
				t.Errorf("StringToTime() = %v, expected %v", result.Time, tt.expected)
			}
		})
	}
}
