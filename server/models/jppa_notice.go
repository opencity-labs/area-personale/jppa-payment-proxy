package models

type JppaNoticeRequest struct {
	Debit   *JppaNoticeRequestDebit `json:"chiaviDebito"`
	Printer string                  `json:"idInstallazione"` // tenant.printer_code
	Notice  string                  `json:"numeroAvviso"`    // payment.Payment.IUV + 3
}

type JppaNoticeRequestDebit struct {
	Tenant      string `json:"codiceIpaEnte"`    // tenant.code
	ServiceKind string `json:"codiceTipoDebito"` // sevice.kind
	PaymentID1  string `json:"idPosizione"`      // payment.iud
	PaymentID2  string `json:"idDebito"`         // payment.iud
	ServiceCode string `json:"codiceServizio"`   // service.code
}

type JppaNoticeResponse struct {
	Result string `json:"esito"` // OK o ERROR
	Notice string `json:"fileBase64Encoded"`
}
