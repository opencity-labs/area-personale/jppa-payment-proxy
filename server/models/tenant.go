package models

type Tenant struct {
	ID                      string `json:"id"`
	Active                  bool   `json:"active"`
	Production              bool   `json:"production"`
	Code                    string `json:"code"`
	Name                    string `json:"name"`
	TaxIdentificationNumber string `json:"tax_identification_number"`
	PaymentsEndpoint        string `json:"payments_endpoint"`
	PaymentsUsername        string `json:"payments_username"`
	PaymentsPassword        string `json:"payments_password"`
	PrinterEndpoint         string `json:"printer_endpoint"`
	PrinterUsername         string `json:"printer_username"`
	PrinterPassword         string `json:"printer_password"`
	PrinterCode             string `json:"printer_code"`
	IUVAutogenerateEnabled  bool   `json:"iuv_autogenerate"`
	IUVAux                  string `json:"iuv_aux"`
	SegregationCode         string `json:"segregation_code"`
	IstatCode               string `json:"istat_code"`
	IUVPrefix               string `json:"iuv_prefix"`
}
