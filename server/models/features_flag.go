package models

type ImportAllDues struct {
	Tenants []EnabledTenant `json:"tenants"`
}

type EnabledTenant struct {
	ID string `json:"id"`
}
