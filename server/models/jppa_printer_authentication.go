package models

type JppaPrinterAuthenticationRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type JppaPrinterAuthenticationResponse struct {
	Accesstoken string `json:"token"`
}
