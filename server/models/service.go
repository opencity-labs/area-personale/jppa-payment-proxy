package models

import "encoding/json"

type Service struct {
	ID               string           `json:"id"`
	TenantID         string           `json:"tenant_id"`
	Active           bool             `json:"active"`
	Code             string           `json:"code"`              // servizio
	Kind             string           `json:"kind"`              // codice tipo debito
	Description      string           `json:"description"`       // descrizione usata come causale
	EntryCode        string           `json:"entry_code"`        // codiceAccertamento quando bilancio disattivato
	EntryDescription string           `json:"entry_description"` // descrizioneAccertamento quando bilancio disattivato
	Splitted         bool             `json:"splitted"`
	Split            []ServiceSplit   `json:"split"`
	PaymentType      string           `json:"payment_type"`
	RemoteCollection RemoteCollection `json:"remote_collection"`
	Amount           float64          `json:"amount"`    // deve essere maggiore di 0
	Reason           string           `json:"reason"`    // obbligatorio, massimo 255 caratteri
	ExpireAt         string           `json:"expire_at"` // deve essere futura e valida
	Receiver         Receiver         `json:"receiver"`
	CollectionData   string           `json:"collection_data"` // tassonomia pagoPA dei pagamenti
}

type ServiceSplit struct {
	Code             string          `json:"split_code"`
	Amount           float64         `json:"split_amount" description:"..." format:"float"`
	EntryCode        string          `json:"split_entry_code"`        // codiceAccertamento
	EntryDescription string          `json:"split_entry_description"` // descrizioneAccertamento
	Meta             json.RawMessage `json:"meta"`                    // Non obbligatorio

}

type RemoteCollection struct {
	ID   string `json:"id"`
	Type string `json:"type"` // application, service, altro
}

// Receiver rappresenta il beneficiario del pagamento.
type Receiver struct {
	TaxIdentificationNumber string `json:"tax_identification_number"`
	Name                    string `json:"name"` // massimo 255 caratteri
}
