package models

type JppaVerifyRequest struct {
	TenantCode  string                  `json:"codIpaRichiedente"` // tenant.code
	ServiceCode string                  `json:"codiceServizio"`    // service.code
	Debit       *JppaVerifyRequestDebit `json:"chiaveDebitoDto"`
}
type JppaVerifyRequestDebit struct {
	TenantCode  string `json:"codEnteCreditore"` // tenant.code
	ServiceKind string `json:"codiceTipoDebito"` // service.kind
	PaymentID1  string `json:"iDeb"`
	PaymentID2  string `json:"iPos"`
}

type JppaVerifyResponse struct {
	Details []*JppaVerifyResponseDetail `json:"listaInfoPagamentoTelematicoDto"`
}

type JppaVerifyResponseDetail struct {
	PaidAt int64  `json:"dataPagamento"`
	Result string `json:"esitoRichiestaPagamento"`
	IUV    string `json:"identificativoUnivocoVersamento"`
}
