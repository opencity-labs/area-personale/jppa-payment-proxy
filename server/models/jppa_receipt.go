package models

type JppaReceiptRequest struct {
	Printer string `json:"idInstallazione"` // tenant.printer_code
	Tenant  string `json:"codiceIpa"`       // tenant.code
	IUV     string `json:"iuv"`             // payment.Payment.IUV
}

type JppaReceiptResponse struct {
	Result  string `json:"esito"` // OK o ERROR
	Receipt string `json:"fileBase64Encoded"`
}
