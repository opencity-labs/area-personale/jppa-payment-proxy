package models

type Schema struct {
	Display    string            `json:"display,omitempty"`
	Components []SchemaComponent `json:"components,omitempty"`
}

type SchemaComponent struct {
	Label                  string                      `json:"label,omitempty"`
	Description            string                      `json:"description,omitempty"`
	Placeholder            string                      `json:"placeholder,omitempty"`
	Spellcheck             bool                        `json:"spellcheck,omitempty"`
	Attributes             *SchemaComponentAttribute   `json:"attributes,omitempty"`
	Validate               *SchemaComponentValidate    `json:"validate,omitempty"`
	Key                    string                      `json:"key,omitempty"`
	Type                   string                      `json:"type,omitempty"`
	Input                  bool                        `json:"input,omitempty"`
	Hidden                 bool                        `json:"hidden,omitempty"`
	DefaultValue           any                         `json:"defaultValue,omitempty"`
	TableView              bool                        `json:"tableView,omitempty"`
	ShowValidations        bool                        `json:"showValidations,omitempty"`
	Reorder                bool                        `json:"reorder,omitempty"`
	AddAnotherPosition     string                      `json:"addAnotherPosition,omitempty"`
	LayoutFixed            bool                        `json:"layoutFixed,omitempty"`
	EnableRowGroups        bool                        `json:"enableRowGroups,omitempty"`
	InitEmpty              bool                        `json:"initEmpty,omitempty"`
	Conditional            *SchemaComponentConditional `json:"conditional,omitempty"`
	Components             []*SchemaComponent          `json:"components,omitempty"`
	Mask                   bool                        `json:"mask,omitempty"`
	Delimiter              bool                        `json:"delimiter,omitempty"`
	RequireDecimal         bool                        `json:"requireDecimal,omitempty"`
	InputFormat            any                         `json:"inputFormat,omitempty"`
	TruncateMultipleSpaces bool                        `json:"truncateMultipleSpaces,omitempty"`
	Widget                 string                      `json:"widget,omitempty"`
	CalculateValue         string                      `json:"calculateValue,omitempty"`
	Data                   *SchemaComponentData        `json:"data,omitempty"`
}

type SchemaComponentAttribute struct {
	Readonly string `json:"readonly,omitempty"`
}

type SchemaComponentValidate struct {
	Required  bool    `json:"required,omitempty"`
	Pattern   string  `json:"pattern,omitempty"`
	MaxLength int     `json:"maxLength,omitempty"`
	Min       float64 `json:"min,omitempty"`
}

type SchemaComponentConditional struct {
	Show bool   `json:"show,omitempty"`
	When string `json:"when,omitempty"`
	Eq   string `json:"eq,omitempty"`
}

type SchemaComponentDefaultValueSplit struct {
	SplitCode             string  `json:"split_code"`
	SplitAmount           float64 `json:"split_amount" description:"..." format:"float"`
	SplitEntryDescription string  `json:"split_entry_description"`
	SplitEntryCode        string  `json:"split_entry_code"`
}

type SchemaComponentData struct {
	Values []*SchemaComponentDataValue `json:"values,omitempty"`
}

type SchemaComponentDataValue struct {
	Label string `json:"label,omitempty"`
	Value string `json:"value,omitempty"`
}

type Data struct {
	Values []Option `json:"values,omitempty"`
}

type Option struct {
	Value string `json:"value"`
	Label string `json:"label"`
}

var TenantSchema = Schema{
	Display: "form",
	Components: []SchemaComponent{
		{Label: "Nome dell'ente", Placeholder: "Inserisci il nome", Validate: &SchemaComponentValidate{Required: true, MaxLength: 255}, Key: "name", Type: "textfield", Input: true, TableView: true},
		{Label: "Codice Fiscale dell'ente", Placeholder: "Inserisci il codice fiscale", Validate: &SchemaComponentValidate{Required: true, Pattern: "^[A-Z0-9]{11,16}$"}, Key: "tax_identification_number", Type: "textfield", Input: true, TableView: true},
		{Label: "UUID del Tenant", Hidden: true, Placeholder: "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX", Spellcheck: false, Attributes: &SchemaComponentAttribute{Readonly: "readonly"}, Validate: &SchemaComponentValidate{Required: true}, Key: "id", Type: "textfield", Input: true, TableView: true},
		{Label: "Abilitato", Key: "active", Type: "checkbox", Input: true, Hidden: true, DefaultValue: true, TableView: false},
		{Label: "Ambiente di produzione", Placeholder: "Flaggare questo campo per abilitare l'ambiente di produzione", Key: "production", Type: "checkbox", Input: true, DefaultValue: false, TableView: false},
		{Label: "Codice Jppa", Placeholder: "Codice ente assegnato da Jppa", Validate: &SchemaComponentValidate{Required: true}, Key: "code", Type: "textfield", Input: true, TableView: true},
		{Label: "Nome Jppa", Placeholder: "Nome ente assegnato da Jppa", Validate: &SchemaComponentValidate{Required: true}, Key: "name", Type: "textfield", Input: true, TableView: true},
		{Label: "Codice fiscale ente", Placeholder: "Codice fiscale ente", Validate: &SchemaComponentValidate{Required: true}, Key: "tax_number", Type: "textfield", Input: true, TableView: true},
		{Label: "Endpoint API Jppa", Placeholder: "Endpoint API assegnato da Jppa", Validate: &SchemaComponentValidate{Required: true}, Key: "payments_endpoint", Type: "textfield", Input: true, TableView: true},
		{Label: "Username API Jppa", Placeholder: "Username API assegnato da Jppa", Validate: &SchemaComponentValidate{Required: true}, Key: "payments_username", Type: "textfield", Input: true, TableView: true},
		{Label: "Password API Jppa", Placeholder: "Password API assegnato da Jppa", Validate: &SchemaComponentValidate{Required: true}, Key: "payments_password", Type: "password", Input: true, TableView: true},
		{Label: "Endpoint PDF Jppa", Placeholder: "Endpoint PDF assegnato da Jppa", Validate: &SchemaComponentValidate{Required: true}, Key: "printer_endpoint", Type: "textfield", Input: true, TableView: true},
		{Label: "Username PDF Jppa", Placeholder: "Username PDF assegnato da Jppa", Validate: &SchemaComponentValidate{Required: true}, Key: "printer_username", Type: "textfield", Input: true, TableView: true},
		{Label: "Password PDF Jppa", Placeholder: "Password PDF assegnato da Jppa", Validate: &SchemaComponentValidate{Required: true}, Key: "printer_password", Type: "password", Input: true, TableView: true},
		{Label: "Codice PDF Jppa", Placeholder: "Codice PDF assegnato da Jppa", Validate: &SchemaComponentValidate{Required: true}, Key: "printer_code", Type: "textfield", Input: true, TableView: true},
		{Label: "Autogenerazione IUV", Key: "iuv_autogenerate", Type: "checkbox", Input: true, DefaultValue: false, TableView: false},
		{Label: "IUV Aux digit", Placeholder: "IUV aux digit assegnato da JPPA", Validate: &SchemaComponentValidate{Required: true}, Key: "iuv_aux", Type: "textfield", Input: true, TableView: true, Conditional: &SchemaComponentConditional{Show: true, When: "iuv_autogenerate", Eq: "true"}},
		{Label: "Codice segregazione", Placeholder: "Codice segregazione assegnato da JPPA", Validate: &SchemaComponentValidate{Required: true}, Key: "segregation_code", Type: "textfield", Input: true, TableView: true, Conditional: &SchemaComponentConditional{Show: true, When: "iuv_autogenerate", Eq: "true"}},
		{Label: "Codice Istat/ente", Placeholder: "Codice Istat/ente assegnato da JPPA", Validate: &SchemaComponentValidate{Required: true}, Key: "istat_code", Type: "textfield", Input: true, TableView: true, Conditional: &SchemaComponentConditional{Show: true, When: "iuv_autogenerate", Eq: "true"}},
		{Label: "Prefisso IUV servizio", Placeholder: "Prefisso IUV servizio servizio assegnato da JPPA", Validate: &SchemaComponentValidate{Required: true}, Key: "iuv_prefix", Type: "textfield", Input: true, TableView: true, Conditional: &SchemaComponentConditional{Show: true, When: "iuv_autogenerate", Eq: "true"}},
		{Label: "Attiva l'importazione automatica dei pagamenti dovuti", Key: "active_import", Type: "checkbox", Input: true, Hidden: false, DefaultValue: true, TableView: false},
		{Label: "Salva", ShowValidations: false, Key: "submit", Type: "button", Input: true, TableView: false},
	},
}

var ServiceSchema = Schema{
	Display: "form",
	Components: []SchemaComponent{
		{Label: "UUID del Servizio", Hidden: true, Placeholder: "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX", Spellcheck: false, Attributes: &SchemaComponentAttribute{Readonly: "readonly"}, Validate: &SchemaComponentValidate{Required: true}, Key: "id", Type: "textfield", Input: true, TableView: true},
		{Label: "UUID del Tenant", Hidden: true, Placeholder: "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX", Spellcheck: false, Attributes: &SchemaComponentAttribute{Readonly: "readonly"}, Validate: &SchemaComponentValidate{Required: true}, Key: "tenant_id", Type: "textfield", Input: true, TableView: true},
		{Label: "Abilitato", Hidden: true, DefaultValue: true, Key: "active", Type: "checkbox", Input: true, TableView: false},
		{Label: "Servizio", Placeholder: "OPENCITY", Description: "Questo codice è fornito all'ente da Jppa", Validate: &SchemaComponentValidate{Required: true}, Key: "code", Type: "textfield", Input: true, TableView: true},
		{Label: "Codice tipo debito", Placeholder: "VOTIVE", Description: "Codice tipo debito", Validate: &SchemaComponentValidate{Required: true}, Key: "kind", Type: "textfield", Input: true, TableView: true},
		{Label: "Descrizione del servizio", Placeholder: "Multa per eccesso di velocità", Validate: &SchemaComponentValidate{Required: true}, Key: "description", Type: "textfield", Input: true, TableView: true},
		{Label: "Codice accertamento", Placeholder: "123", Description: "Codice accertamento quando bilancio disattivato", Key: "entry_code", Type: "textfield", Input: true, TableView: true},
		{Label: "Descrizione accertamento", Placeholder: "123/2024", Description: "Descrizione accertamento quando bilancio disattivato", Key: "entry_description", Type: "textfield", Input: true, TableView: true},
		{Label: "Tipo di Pagamento", Key: "payment_type", Type: "select", Input: true, TableView: true, Validate: &SchemaComponentValidate{Required: true}, Data: &SchemaComponentData{Values: []*SchemaComponentDataValue{{Label: "PagoPA", Value: "pagopa"}, {Label: "Marca da bollo digitale", Value: "stamp"}}}},
		{Label: "ID Collezione di origine del pagamento", Key: "remote_collection.id", Type: "textfield", Input: true, TableView: true, Validate: &SchemaComponentValidate{Required: true}},
		{Label: "Tipo Collezione di origine del pagamento", Key: "remote_collection.type", Type: "select", Input: true, TableView: true, Validate: &SchemaComponentValidate{Required: true}, Data: &SchemaComponentData{Values: []*SchemaComponentDataValue{{Value: "application", Label: "Applicazione"}, {Value: "service", Label: "Servizio"}, {Value: "altro", Label: "Altro"}}}},
		{Label: "Importo Pagamento", Key: "amount", Type: "number", Input: true, TableView: true, Validate: &SchemaComponentValidate{Required: true, Min: 0.01}},
		{Label: "Causale Pagamento", Key: "reason", Type: "textfield", Input: true, TableView: true, Validate: &SchemaComponentValidate{Required: true, MaxLength: 255}},
		{Label: "Data Scadenza Pagamento", Key: "expire_at", Type: "datetime", Input: true, TableView: true, Validate: &SchemaComponentValidate{Required: true}},
		{Label: "Codice Fiscale Beneficiario", Key: "receiver.tax_identification_number", Type: "textfield", Input: true, TableView: true, Validate: &SchemaComponentValidate{Required: true}},
		{Label: "Nome Beneficiario", Key: "receiver.name", Type: "textfield", Input: true, TableView: true, Validate: &SchemaComponentValidate{Required: true, MaxLength: 255}},
		{Label: "Categoria pagamento", Key: "collection_data", Type: "textfield", Input: true, TableView: true, Validate: &SchemaComponentValidate{Required: true}},
		{Label: "Bilancio", DefaultValue: true, Key: "splitted", Type: "checkbox", Input: true, TableView: true},
		{Label: "Dettagli bilancio", Reorder: false, AddAnotherPosition: "bottom", LayoutFixed: false, EnableRowGroups: false, InitEmpty: true, Conditional: &SchemaComponentConditional{Show: true, When: "splitted", Eq: "true"},
			DefaultValue: []*SchemaComponentDefaultValueSplit{{SplitCode: "c_1", SplitAmount: 0, SplitEntryDescription: "123", SplitEntryCode: "123/2024"}}, Key: "split", Type: "datagrid", Input: true,
			TableView: true, Components: []*SchemaComponent{
				{Label: "Codice", Placeholder: "c_1", Description: "Identificativo univoco della voce di bilancio. Testo libero", Validate: &SchemaComponentValidate{Required: true}, Key: "split_code", Type: "textfield", Input: true, TableView: true},
				{Label: "Importo", Placeholder: "16.00", Description: "Importo della voce di bilancio. NB: La somma degli importi delle voci DEVE equivalere all'importo totale", Validate: &SchemaComponentValidate{Required: true}, Mask: false, Delimiter: false, RequireDecimal: false, InputFormat: "plain", TruncateMultipleSpaces: false, Key: "split_amount", Type: "number", Input: true, TableView: true},
				{Label: "Codice voce", Placeholder: "123 one", Description: "Codice della voce di bilancio", Validate: &SchemaComponentValidate{Required: true}, Key: "split_entry_code", Type: "textfield", Input: true, TableView: true},
				{Label: "Descrizione voce", Placeholder: "123/2024", Description: "Descrizione della voce di bilancio", Validate: &SchemaComponentValidate{Required: true}, Key: "split_entry_description", Type: "textfield", Input: true, TableView: true}}},
		{Label: "hidden", CalculateValue: "if (!data.split || data.split == 'undefined') {\n  data.split = []\n} else if (typeof data.split==='object' && Object.keys(data.split).length === 0) {\n  data.split = [];\n}", Key: "hidden", Type: "hidden", Input: true, TableView: false},
		{Label: "Salva", ShowValidations: false, Key: "submit", Type: "button", Input: true, TableView: false},
	},
}
