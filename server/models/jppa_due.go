package models

type JppaDueRequest struct {
	TenantCode  string                 `json:"codiceIPA"`      // tenant.Code
	ServiceCode string                 `json:"codiceServizio"` // service.Code
	Debits      []*JppaDueRequestDebit `json:"dovuti"`         // json string
}

type JppaDueRequestDebit struct {
	DebitCtx     string                  `json:"contestoDovuto"` // sempre MONOBENEFICIARIO
	Details      []*JppaDueRequestDetail `json:"dettaglioDovuto"`
	NoticeNumber *NoticeNumber           `json:"numeroAvviso"`
	Payer        *JppaDueRequestPayer    `json:"testataDovuto"`
}

type JppaDueRequestDetail struct {
	Reason          string                 `json:"causaleDebito"`        // service.kind
	TenantCode      string                 `json:"codiceIpaCreditore"`   // tenant.code
	DueID           string                 `json:"codiceLotto"`          // payment.id
	DueKind         string                 `json:"codiceTipoDebito"`     // service.kind
	ActivedAt       string                 `json:"dataAttualizzazione"`  // YYYY-MM-DDT00:00:00.000Z
	ExpireAt        string                 `json:"dataFineValidita"`     // YYYY-MM-DDT00:00:00.000Z
	CreatedAt       string                 `json:"dataInizioValidita"`   // YYYY-MM-DDT00:00:00.000Z
	PaymentExpireAt string                 `json:"dataLimitePagabilita"` // YYYY-MM-DDT00:00:00.000Z
	Splits          []*JppaDueRequestSplit `json:"datiAccertamento"`
	Group           int32                  `json:"gruppo"` // 1
	PaymentID       string                 `json:"idDeb"`  // payment.iud
	Amount          float64                `json:"importoDebito"`
	Order           int32                  `json:"ordinamento"` // 1
}

type NoticeNumber struct {
	NoticeNumber        string `json:"numeroAvviso"`
	NoticeNumberVersion int    `json:"versioneNumeroAvviso"`
}

type JppaDueRequestSplit struct {
	Year             int32   `json:"annoAccertamento"`        // anno di riferimento del pagamento
	EntryCode        string  `json:"codiceAccertamento"`      // service.entry_code ||  service.split.entry_code
	EntryDescription string  `json:"descrizioneAccertamento"` // service.entry_description ||  service.split.entry_description
	Amount           float64 `json:"importoAccertamento"`
}

type JppaDueRequestPayer struct {
	PayerDetails *JppaDueRequestPayerDetails `json:"datiContribuente"`
	Description  string                      `json:"dettaglioPosizione"` // service.descriptiom
	PaymentID    string                      `json:"idPos"`              // payment.iud
}

type JppaDueRequestPayerDetails struct {
	PayerType                    string `json:"tipoIdentificativoUnivoco"` // "F" fisica o "G" giuridica
	PayerTaxIdentificationNumber string `json:"codiceIdentificativoUnivoco,omitempty"`
	PayerCompanyName             string `json:"ragioneSociale,omitempty"`
	PayerName                    string `json:"nome,omitempty"`
	PayerFamilyName              string `json:"cognome,omitempty"`
	PayerEmail                   string `json:"email,omitempty"`
	PayerStreetName              string `json:"indirizzo,omitempty"`
	PayerBuildingNumber          string `json:"civico,omitempty"`
	PayerPostalCode              string `json:"cap,omitempty"`
	PayerTownName                string `json:"localita,omitempty"`
	PayerCountrySubdivision      string `json:"provincia,omitempty"`
	PayerCountry                 string `json:"nazione,omitempty"`
}

type JppaDueResponse struct {
	// ErrorCode *string                 `json:"errorCode"`                // se presente chiamata error
	Debits []*JppaDueResponseDebit `json:"avvisiPagamentoResultDto"` // se presente chiamata success
}

type JppaDueResponseDebit struct {
	Result *JppaDueResponseResult `json:"esito"`
	Notice *JppaDueResponseNotice `json:"numeroAvvisoDto"`
}

type JppaDueResponseResult struct {
	Result  string `json:"esito"` // se ok == "OK"
	Message string `json:"messaggio"`
}

type JppaDueResponseNotice struct {
	NoticeCode string `json:"numeroAvviso"`
}
