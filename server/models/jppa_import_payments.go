package models

type ImportPaymentsRequest struct {
	TaxonomyCode     *string    `json:"codiceDiTassonomia,omitempty"`
	DebtorFiscalCode string     `json:"codiceFiscaleDebitore"`
	EntityCode       string     `json:"codiceIpa"`
	DebtTypeCode     *string    `json:"codiceTipoDebito,omitempty"`
	PaymentDateRange *DateRange `json:"dataDiPagamento,omitempty"`
	DueDateRange     *DateRange `json:"dataDiScadenza,omitempty"`
	ItemsPerPage     *int       `json:"elementiInPagina,omitempty"`
	Payment          *int       `json:"pagamento,omitempty"`
	Page             *int       `json:"pagina,omitempty"`
}

type ImportPaymentsResponse struct {
	Debts      []Debt `json:"debiti"`
	TotalItems int    `json:"numeroTotaleDiElementi"`
}

type DateRange struct {
	BeginDate string `json:"beginDate"`
	EndDate   string `json:"endDate"`
}

type Debt struct {
	Base64Receipt           string  `json:"base64RT"`
	PaymentReason           string  `json:"causalePagamento"`
	TaxonomyCode            string  `json:"codiceDiTassonomia"`
	EntityCode              string  `json:"codiceIpaEnte"`
	ServiceCode             string  `json:"codiceServizio"`
	DebtTypeCode            string  `json:"codiceTipoDebito"`
	PaymentDate             string  `json:"dataPagamento"`
	DueDate                 string  `json:"dataScadenzaPagamento"`
	TaxonomyDescription     string  `json:"descrizioneTassonomia"`
	Group                   string  `json:"gruppo"`
	IDeb                    string  `json:"iDeb"`
	IPosDeb                 string  `json:"iPosDeb"`
	UniquePaymentIdentifier string  `json:"identificativoUnivocoVersamento"`
	DebtAmount              float64 `json:"importoDebito"`
	PaymentMethod           string  `json:"modalitaPagamento"`
	NoticeNumber            string  `json:"numeroAvviso"`
	Paid                    bool    `json:"pagato"`
	PaymentStatus           string  `json:"statoPagamento"`
}
