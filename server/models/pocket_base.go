package models

type PbAuthpayload struct {
	Username string `json:"identity"`
	Password string `json:"password"`
}

type PbAuthRecord struct {
	Avatar          string `json:"avatar"`
	CollectionID    string `json:"collectionId"`
	CollectionName  string `json:"collectionName"`
	Created         string `json:"created"`
	Email           string `json:"email"`
	EmailVisibility bool   `json:"emailVisibility"`
	ID              string `json:"id"`
	Name            string `json:"name"`
	Note            string `json:"note"`
	Updated         string `json:"updated"`
	Username        string `json:"username"`
	Verified        bool   `json:"verified"`
}

type PbTokenResponse struct {
	Record PbAuthRecord `json:"record"`
	Token  string       `json:"token"`
}

type PbResponse struct {
	Page       int    `json:"page"`
	PerPage    int    `json:"perPage"`
	TotalItems int    `json:"totalItems"`
	TotalPages int    `json:"totalPages"`
	Items      []Item `json:"items"`
}

type Item struct {
	AuthType         []string `json:"auth_type"`
	CasLoginURL      string   `json:"cas_login_url"`
	CasValidationURL string   `json:"cas_validation_url"`
	CollectionID     string   `json:"collectionId"`
	CollectionName   string   `json:"collectionName"`
	Created          string   `json:"created"`
	CurrentURL       string   `json:"current_url"`
	Customer         string   `json:"customer"`
	DBName           string   `json:"db_name"`
	DeployDBCreation string   `json:"deploy_db_creation"`
	DeploySDCInit    string   `json:"deploy_sdc_init"`
	Description      string   `json:"description"`
	Host             string   `json:"host"`
	ID               string   `json:"id"`
	Identifier       string   `json:"identifier"`
	Languages        []string `json:"languages"`
	Owner            string   `json:"owner"`
	PaymentGateways  []string `json:"payment_gateways"`
	ProductionURL    string   `json:"production_url"`
	SingleLogoutURL  string   `json:"single_logout_url"`
	Stack            string   `json:"stack"`
	Status           string   `json:"status"`
	TemporaryURL     string   `json:"temporary_url"`
	Updated          string   `json:"updated"`
	UUID             string   `json:"uuid"`
}
