package models

import "time"

type Address struct {
	Address      string `json:"address"`
	Country      string `json:"country"`
	County       string `json:"county"`
	Municipality string `json:"municipality"`
	PostCode     string `json:"post_code"`
}

type User struct {
	AppID          string    `json:"app_id"`
	AppVersion     string    `json:"app_version"`
	CountryOfBirth string    `json:"country_of_birth"`
	CountyOfBirth  string    `json:"county_of_birth"`
	CreatedAt      time.Time `json:"created_at"`
	DateOfBirth    time.Time `json:"date_of_birth"`
	Domicile       Address   `json:"domicile"`
	Email          string    `json:"email"`
	EventCreatedAt time.Time `json:"event_created_at"`
	EventID        string    `json:"event_id"`
	EventVersion   int       `json:"event_version"`
	FamilyName     string    `json:"family_name"`
	FullName       string    `json:"full_name"`
	Gender         string    `json:"gender"`
	GivenName      string    `json:"given_name"`
	ID             string    `json:"id"`
	MobilePhone    string    `json:"mobile_phone"`
	Path           string    `json:"path"`
	PlaceOfBirth   string    `json:"place_of_birth"`
	Residency      Address   `json:"residency"`
	Role           string    `json:"role"`
	SourceType     string    `json:"source_type"`
	SpidCode       string    `json:"spid_code"`
	TaxCode        string    `json:"tax_code"`
	Telephone      string    `json:"telephone"`
	TenantID       string    `json:"tenant_id"`
	Timestamp      time.Time `json:"timestamp"`
	UpdatedAt      time.Time `json:"updated_at"`
	XForwardedFor  string    `json:"x-forwarded-for"`
}
