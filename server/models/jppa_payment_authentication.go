package models

type JppaPaymentAuthenticationRequest struct {
	MsgID    string `json:"idMessaggio"`
	Code     string `json:"identificativoEnte"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type JppaPaymentAuthenticationResponse struct {
	Accesstoken string `json:"token"`
	Result      string `json:"esito"`
	ErrorKind   string `json:"descrizioneErrore"`
}
