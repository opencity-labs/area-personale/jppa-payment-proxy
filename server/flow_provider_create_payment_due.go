package server

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server/models"
)

type FlowProviderCreatePaymentDue struct {
	Name       string
	Sctx       *ServerContext
	Payment    *models.Payment
	Service    *models.Service
	Tenant     *models.Tenant
	AuthToken  string
	Request    *http.Request
	Client     *http.Client
	Err        error
	Msg        string
	Status     bool
	Result     *models.JppaDueResponse
	NoticeCode string
}

func (flow *FlowProviderCreatePaymentDue) Exec() bool {
	flow.Name = "ProviderCreatePaymentDue"
	flow.Status = true &&
		flow.preparePayment() &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderCreatePaymentDue) preparePayment() bool {
	referenceYear, err := strconv.Atoi(flow.Payment.CreatedAt.Format("2006"))
	if err != nil {
		flow.Err = err
		flow.Msg = "reference year conversion error while preparing due request"
		return false
	}

	paymentDetailSplit := []*models.PaymentDetailSplit{}

	if flow.Service.Splitted && flow.Service.Split != nil {
		// fmt.Println("AAAA")
		// aaaa, _ := json.Marshal(flow.Service.Split)
		// fmt.Println(string(aaaa))

		for _, serviceSplit := range flow.Service.Split {
			serviceSplitAmount := serviceSplit.Amount

			paymentSplit := &models.PaymentDetailSplit{
				Code:   serviceSplit.Code,
				Amount: &serviceSplitAmount,
				Meta: &models.PaymentDetailSplitMeta{
					Year:             int32(referenceYear),
					EntryCode:        serviceSplit.EntryCode,
					EntryDescription: serviceSplit.EntryDescription,
				},
			}
			paymentDetailSplit = append(paymentDetailSplit, paymentSplit)
		}
	}

	// fmt.Println("BBBB")
	// bbbb, _ := json.Marshal(paymentDetailSplit)
	// fmt.Println(string(bbbb))

	if flow.Payment.Payment.Split != nil {
		for _, incomingPaymentSplit := range flow.Payment.Payment.Split {
			if incomingPaymentSplit.Amount == nil {
				for index, paymentSplit := range paymentDetailSplit {
					if incomingPaymentSplit.Code == paymentSplit.Code {
						paymentDetailSplit = append(paymentDetailSplit[:index], paymentDetailSplit[index+1:]...)
					}
				}
			} else {
				for _, paymentSplit := range paymentDetailSplit {
					if incomingPaymentSplit.Code == paymentSplit.Code {
						paymentSplit.Amount = incomingPaymentSplit.Amount
					}
				}
			}
		}
	}

	// fmt.Println("CCCC")
	// cccc, _ := json.Marshal(paymentDetailSplit)
	// fmt.Println(string(cccc))
	// fmt.Println("DDDD")
	// dddd, _ := json.Marshal(flow.Payment.Payment.Split)
	// fmt.Println(string(dddd))

	flow.Payment.Payment.Split = paymentDetailSplit

	var totalAmount float64 = 0.0
	for _, paymentSplit := range flow.Payment.Payment.Split {
		totalAmount += *paymentSplit.Amount
	}

	// fmt.Println("EEEE")
	// eeee, _ := json.Marshal(flow.Payment.Payment.Split)
	// fmt.Println(string(eeee))

	if len(flow.Payment.Payment.Split) > 0 && flow.Payment.Payment.Amount != totalAmount {
		flow.Msg = "total amount and split amounts sum mismatching"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	return true
}

func (flow *FlowProviderCreatePaymentDue) prepareRequest() bool {
	if flow.Tenant.IUVAutogenerateEnabled {
		return flow.prepareRequestWithAutoGenIuv()
	}
	return flow.prepareRequestWithoutAutoGenIuv()
}

func (flow *FlowProviderCreatePaymentDue) prepareRequestWithoutAutoGenIuv() bool {
	referenceYear, err := strconv.Atoi(flow.Payment.CreatedAt.Format("2006"))
	if err != nil {
		flow.Err = err
		flow.Msg = "reference year conversion error while preparing due request"
		return false
	}

	var payerType, payerCompanyName string
	if flow.Payment.Payer.Type == "human" {
		payerType = "F"
		payerCompanyName = ""
	} else {
		payerType = "G"
		payerCompanyName = flow.Payment.Payer.Name + " " + flow.Payment.Payer.FamilyName
	}

	dueSplits := []*models.JppaDueRequestSplit{}

	if flow.Payment.Payment.Split != nil && len(flow.Payment.Payment.Split) != 0 {
		for _, paymentSplit := range flow.Payment.Payment.Split {

			dueSplit := &models.JppaDueRequestSplit{
				Year:             paymentSplit.Meta.Year,
				EntryCode:        paymentSplit.Meta.EntryCode,
				EntryDescription: paymentSplit.Meta.EntryDescription,
				Amount:           *paymentSplit.Amount,
			}

			dueSplits = append(dueSplits, dueSplit)
		}
	} else {
		dueSplit := &models.JppaDueRequestSplit{
			Year:             int32(referenceYear),
			EntryCode:        flow.Service.EntryCode,
			EntryDescription: flow.Service.EntryDescription,
			Amount:           flow.Payment.Payment.Amount,
		}

		dueSplits = append(dueSplits, dueSplit)
	}

	dueDataRequest := models.JppaDueRequest{
		TenantCode:  flow.Tenant.Code,
		ServiceCode: flow.Service.Code,
		Debits: []*models.JppaDueRequestDebit{
			{
				DebitCtx: "MONOBENEFICIARIO",
				Details: []*models.JppaDueRequestDetail{
					{
						Reason:          flow.Payment.Reason,
						TenantCode:      flow.Tenant.Code,
						DueID:           flow.Payment.ID,
						DueKind:         flow.Service.Kind,
						ActivedAt:       flow.Payment.CreatedAt.Format("2006-01-02T15:04:05Z"),
						CreatedAt:       flow.Payment.CreatedAt.Format("2006-01-02T15:04:05Z"),
						ExpireAt:        flow.Payment.Payment.ExpireAt.Format("2006-01-02T15:04:05Z"),
						PaymentExpireAt: flow.Payment.Payment.ExpireAt.Format("2006-01-02T15:04:05Z"),
						Splits:          dueSplits,
						Group:           1,
						PaymentID:       flow.Payment.ID,
						Amount:          flow.Payment.Payment.Amount,
						Order:           1,
					},
				},
				Payer: &models.JppaDueRequestPayer{
					PayerDetails: &models.JppaDueRequestPayerDetails{
						PayerType:                    payerType,
						PayerTaxIdentificationNumber: flow.Payment.Payer.TaxIdentificationNumber,
						PayerCompanyName:             payerCompanyName,
						PayerName:                    flow.Payment.Payer.Name,
						PayerFamilyName:              flow.Payment.Payer.FamilyName,
						PayerEmail:                   flow.Payment.Payer.Email,
						PayerStreetName:              flow.Payment.Payer.StreetName,
						PayerBuildingNumber:          flow.Payment.Payer.BuildingNumber,
						PayerPostalCode:              flow.Payment.Payer.PostalCode,
						PayerTownName:                flow.Payment.Payer.TownName,
						PayerCountrySubdivision:      flow.Payment.Payer.CountrySubdivision,
						PayerCountry:                 flow.Payment.Payer.Country,
					},
					Description: flow.Service.Description,
					PaymentID:   flow.Payment.ID,
				},
			},
		},
	}

	dueDataRequestJson, err := json.Marshal(dueDataRequest)
	if err != nil {
		flow.Err = err
		flow.Msg = "json marshal error while preparing due request"
		return false
	}

	apiEndpoint := flow.Tenant.PaymentsEndpoint + "/dovuti/v1/inviaDovuti"
	dueReq, err := http.NewRequest("POST", apiEndpoint, bytes.NewReader(dueDataRequestJson))

	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing due request"
		return false
	}
	authorization := "Bearer " + flow.AuthToken
	dueReq.Header.Add("Authorization", authorization)
	dueReq.Header.Add("Content-Type", "application/json")

	flow.Request = dueReq

	return true
}
func (flow *FlowProviderCreatePaymentDue) prepareRequestWithAutoGenIuv() bool {
	referenceYear, err := strconv.Atoi(flow.Payment.CreatedAt.Format("2006"))
	if err != nil {
		flow.Err = err
		flow.Msg = "reference year conversion error while preparing due request"
		return false
	}

	var payerType, payerCompanyName string
	if flow.Payment.Payer.Type == "human" {
		payerType = "F"
		payerCompanyName = ""
	} else {
		payerType = "G"
		payerCompanyName = flow.Payment.Payer.Name + " " + flow.Payment.Payer.FamilyName
	}

	dueSplits := []*models.JppaDueRequestSplit{}

	if flow.Payment.Payment.Split != nil && len(flow.Payment.Payment.Split) != 0 {
		for _, paymentSplit := range flow.Payment.Payment.Split {

			dueSplit := &models.JppaDueRequestSplit{
				Year:             paymentSplit.Meta.Year,
				EntryCode:        paymentSplit.Meta.EntryCode,
				EntryDescription: paymentSplit.Meta.EntryDescription,
				Amount:           *paymentSplit.Amount,
			}

			dueSplits = append(dueSplits, dueSplit)
		}
	} else {
		dueSplit := &models.JppaDueRequestSplit{
			Year:             int32(referenceYear),
			EntryCode:        flow.Service.EntryCode,
			EntryDescription: flow.Service.EntryDescription,
			Amount:           flow.Payment.Payment.Amount,
		}

		dueSplits = append(dueSplits, dueSplit)
	}

	noticeCode, err := flow.GenerateNoticeCode(strconv.Itoa(referenceYear))
	if err != nil {
		flow.Err = err
		flow.Msg = "error generating notice code"
		return false
	}
	flow.Payment.Payment.IUV = GetIuvFromNoticeCode(noticeCode)
	dueDataRequest := models.JppaDueRequest{
		TenantCode:  flow.Tenant.Code,
		ServiceCode: flow.Service.Code,
		Debits: []*models.JppaDueRequestDebit{
			{
				DebitCtx: "MONOBENEFICIARIO",
				Details: []*models.JppaDueRequestDetail{
					{
						Reason:          flow.Payment.Reason,
						TenantCode:      flow.Tenant.Code,
						DueID:           flow.Payment.ID,
						DueKind:         flow.Service.Kind,
						ActivedAt:       flow.Payment.CreatedAt.Format("2006-01-02T15:04:05Z"),
						CreatedAt:       flow.Payment.CreatedAt.Format("2006-01-02T15:04:05Z"),
						ExpireAt:        flow.Payment.Payment.ExpireAt.Format("2006-01-02T15:04:05Z"),
						PaymentExpireAt: flow.Payment.Payment.ExpireAt.Format("2006-01-02T15:04:05Z"),
						Splits:          dueSplits,
						Group:           1,
						PaymentID:       flow.Payment.ID,
						Amount:          flow.Payment.Payment.Amount,
						Order:           1,
					},
				},
				NoticeNumber: &models.NoticeNumber{
					NoticeNumber:        noticeCode,
					NoticeNumberVersion: 1,
				},
				Payer: &models.JppaDueRequestPayer{
					PayerDetails: &models.JppaDueRequestPayerDetails{
						PayerType:                    payerType,
						PayerTaxIdentificationNumber: flow.Payment.Payer.TaxIdentificationNumber,
						PayerCompanyName:             payerCompanyName,
						PayerName:                    flow.Payment.Payer.Name,
						PayerFamilyName:              flow.Payment.Payer.FamilyName,
						PayerEmail:                   flow.Payment.Payer.Email,
						PayerStreetName:              flow.Payment.Payer.StreetName,
						PayerBuildingNumber:          flow.Payment.Payer.BuildingNumber,
						PayerPostalCode:              flow.Payment.Payer.PostalCode,
						PayerTownName:                flow.Payment.Payer.TownName,
						PayerCountrySubdivision:      flow.Payment.Payer.CountrySubdivision,
						PayerCountry:                 flow.Payment.Payer.Country,
					},
					Description: flow.Service.Description,
					PaymentID:   flow.Payment.ID,
				},
			},
		},
	}

	dueDataRequestJson, err := json.Marshal(dueDataRequest)
	if err != nil {
		flow.Err = err
		flow.Msg = "json marshal error while preparing due request"
		return false
	}

	apiEndpoint := flow.Tenant.PaymentsEndpoint + "/dovuti/v1/inviaDovuti"
	dueReq, err := http.NewRequest("POST", apiEndpoint, bytes.NewReader(dueDataRequestJson))

	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing due request"
		return false
	}
	authorization := "Bearer " + flow.AuthToken
	dueReq.Header.Add("Authorization", authorization)
	dueReq.Header.Add("Content-Type", "application/json")

	flow.Request = dueReq

	return true
}
func (flow *FlowProviderCreatePaymentDue) GenerateNoticeCode(referenceYear string) (string, error) {
	if len(referenceYear) < 2 {
		return "", errors.New("invalid referenceYear for generating notice code")
	}
	lastTwoDigits := referenceYear[len(referenceYear)-2:]
	tenant := flow.Tenant
	noticeCode, err := GenerateNoticeCode(flow.Sctx, tenant.IUVAux, lastTwoDigits, tenant.SegregationCode, tenant.IstatCode, tenant.IUVPrefix)
	if err != nil {
		return "", errors.New("error generating notice code: " + err.Error())
	}
	return noticeCode, nil
}

func (flow *FlowProviderCreatePaymentDue) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("create_payment_due"))

	flow.Client = &http.Client{}

	dueRes, err := flow.Client.Do(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting due request"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}
	defer dueRes.Body.Close()
	dueBody, err := io.ReadAll(dueRes.Body)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while reading due response"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	flow.Result = &models.JppaDueResponse{}
	err = json.Unmarshal(dueBody, flow.Result)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error while reading due response"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	if flow.Result.Debits == nil || len(flow.Result.Debits) == 0 {
		flow.Err = err
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	if flow.Result.Debits[0].Result == nil || flow.Result.Debits[0].Result.Result != "OK" {
		flow.Err = err
		flow.Msg = "provider did not give us a debit"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	if flow.Result.Debits[0].Notice == nil || flow.Result.Debits[0].Notice.NoticeCode == "" {
		flow.Err = err
		flow.Msg = "provider did not give us a notice"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.NoticeCode = flow.Result.Debits[0].Notice.NoticeCode

	return true
}
