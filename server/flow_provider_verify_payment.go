package server

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server/models"
)

type FlowProviderVerifyPayment struct {
	Name      string
	Sctx      *ServerContext
	Payment   *models.Payment
	Service   *models.Service
	Tenant    *models.Tenant
	AuthToken string
	Request   *http.Request
	Client    *http.Client
	Err       error
	Msg       string
	Status    bool
	Result    *models.JppaVerifyResponse
	Payed     bool
	PaidAt    models.Time
	IUV       string
}

func (flow *FlowProviderVerifyPayment) Exec() bool {
	flow.Name = "ProviderVerifyPayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderVerifyPayment) prepareRequest() bool {

	verifyDataRequest := models.JppaVerifyRequest{
		Debit: &models.JppaVerifyRequestDebit{
			TenantCode:  flow.Tenant.Code,
			ServiceKind: flow.Service.Kind,
			PaymentID1:  flow.Payment.ID,
			PaymentID2:  flow.Payment.ID,
		},
		TenantCode:  flow.Tenant.Code,
		ServiceCode: flow.Service.Code,
	}

	verifyDataRequestJson, err := json.Marshal(verifyDataRequest)
	if err != nil {
		flow.Err = err
		flow.Msg = "json marshal error while preparing verify request"
		return false
	}

	apiEndpoint := flow.Tenant.PaymentsEndpoint + "/pagamenti/v1/infoPerDovuto"
	dueReq, err := http.NewRequest("POST", apiEndpoint, bytes.NewReader(verifyDataRequestJson))

	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing due request"
		return false
	}
	authorization := "Bearer " + flow.AuthToken
	dueReq.Header.Add("Authorization", authorization)
	dueReq.Header.Add("Content-Type", "application/json")

	flow.Request = dueReq
	return true
}

func (flow *FlowProviderVerifyPayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_verify"))

	flow.Client = &http.Client{}

	dueRes, err := flow.Client.Do(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting verify request"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}
	defer dueRes.Body.Close()
	dueBody, err := io.ReadAll(dueRes.Body)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while reading verify response"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}

	flow.Result = &models.JppaVerifyResponse{}
	err = json.Unmarshal(dueBody, flow.Result)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error while reading verify response"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}

	timer.ObserveDuration()

	if flow.Result.Details == nil || len(flow.Result.Details) == 0 {
		flow.Payed = false

		return true
	}

	if flow.Result.Details[0].Result != "CONCLUSO_ESEGUITO" || flow.Result.Details[0].PaidAt == 0 || flow.Result.Details[0].IUV == "" {
		flow.Payed = false

		return true
	}

	flow.Payed = true
	flow.PaidAt = models.Time{Time: time.Unix(flow.Result.Details[0].PaidAt/1000, 0)}
	flow.IUV = flow.Result.Details[0].IUV

	return true
}
