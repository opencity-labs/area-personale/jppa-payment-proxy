package server

import (
	"encoding/json"
	"strings"
	"time"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server/models"
)

type FlowEventPaymentReceived struct {
	Name          string
	Sctx          *ServerContext
	Event         *kafka.Message
	Tenant        *models.Tenant
	Service       *models.Service
	StoredPayment *models.Payment
	Err           error
	Msg           string
	Status        bool
	Result        *models.Payment
}

func (flow *FlowEventPaymentReceived) Exec() bool {
	flow.Name = "EventPaymentReceived"

	flow.Status = true &&
		flow.extractPayment() &&
		flow.loadRelatedTenant() &&
		flow.loadRelatedService() &&
		flow.getPaymentValuesFromPaymentOrConfig() &&
		flow.checkPrecondition() &&
		flow.loadStoredPayment() &&
		flow.checkPostcondition()

	if flow.Status {
		MetricsPaymentsReceived.Inc()
	}

	return flow.Status
}

func (flow *FlowEventPaymentReceived) extractPayment() bool {
	eventValue := flow.Event.Value

	flow.Result = &models.Payment{}
	err := json.Unmarshal(eventValue, flow.Result)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	if flow.Result.EventVersion != "2.0" {
		flow.Err = err
		flow.Msg = "unsupported event version"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	if flow.Result.TenantID == "" || flow.Result.ServiceID == "" {
		flow.Err = err
		flow.Msg = "missing tenant_id or service_id"

		MetricsPaymentsValidationError.Inc()

		return false
	}
	if flow.Result.Type != "PAGOPA" && flow.Result.Type != "STAMP" {
		flow.Err = err
		flow.Msg = "missing type"

		MetricsPaymentsValidationError.Inc()

		return false
	}
	if flow.Result.Document != nil && flow.Result.Document.Hash == "" {
		flow.Err = err
		flow.Msg = "missing document hash"

		MetricsPaymentsValidationError.Inc()

		return false
	}
	if flow.Result.Links.Confirm != nil {
		flow.Err = err
		flow.Msg = "missing link confirm "

		MetricsPaymentsValidationError.Inc()

		return false
	}
	if flow.Result.Links.Cancel != nil {
		flow.Err = err
		flow.Msg = "missing link cancel "

		MetricsPaymentsValidationError.Inc()

		return false
	}

	return true
}

func (flow *FlowEventPaymentReceived) loadRelatedTenant() bool {
	tenantsCache := flow.Sctx.TenantsCache()

	tenant, err := tenantsCache.Get(flow.Sctx.Ctx(), flow.Result.TenantID)
	flow.Tenant = tenant
	if err != nil && err.Error() == "value not found in store" {
		flow.Err = err
		flow.Msg = "irrilevant tenant"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get tenant by id error for: " + flow.Result.TenantID

		MetricsPaymentsInternalError.Inc()

		return false
	}

	return true
}

func (flow *FlowEventPaymentReceived) loadRelatedService() bool {
	servicesCache := flow.Sctx.ServicesCache()

	service, err := servicesCache.Get(flow.Sctx.Ctx(), flow.Result.ServiceID)
	flow.Service = service
	if err != nil && err.Error() == "value not found in store" {
		flow.Err = err
		flow.Msg = "irrilevant service"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get service by id error for: " + flow.Result.ServiceID

		MetricsPaymentsInternalError.Inc()

		return false
	}

	return true
}

func (flow *FlowEventPaymentReceived) getPaymentValuesFromPaymentOrConfig() bool {

	if flow.Result.Receiver != nil && (flow.Result.Receiver.TaxIdentificationNumber == "" || flow.Result.Receiver.Name == "") {
		if flow.Service == nil {
			flow.Err = nil
			flow.Msg = "service receiver data is missing"
			MetricsPaymentsValidationError.Inc()
			return false
		}

		if flow.Service.Receiver.Name == "" || flow.Service.Receiver.TaxIdentificationNumber == "" {
			flow.Err = nil
			flow.Msg = "missing tax_identification_number or name for payment receiver"
			MetricsPaymentsValidationError.Inc()

			return false
		}
		if flow.Result.Receiver.TaxIdentificationNumber == "" {
			flow.Result.Receiver.TaxIdentificationNumber = flow.Service.Receiver.TaxIdentificationNumber
		}
		if flow.Result.Receiver.Name == "" {
			flow.Result.Receiver.Name = flow.Service.Receiver.Name
		}
	}

	if flow.Service.PaymentType == "" {
		flow.Err = nil
		flow.Msg = "missing pagopa_category from config"
		MetricsPaymentsValidationError.Inc()

		return false
	}
	flow.Result.PagopaCategory = flow.Service.PaymentType

	if flow.Result.Reason == "" {
		if flow.Service.Reason == "" {
			flow.Err = nil
			flow.Msg = "missing reason"

			MetricsPaymentsValidationError.Inc()

			return false
		} else {
			flow.Result.Reason = flow.Service.Reason
		}
	}

	if flow.Result.Payment.Amount == 0 {
		if flow.Service.Amount == 0 {
			flow.Err = nil
			flow.Msg = "missing amount"

			MetricsPaymentsValidationError.Inc()

			return false
		} else {
			flow.Result.Payment.Amount = flow.Service.Amount
		}
	}

	if flow.Result.Payment.ExpireAt.IsZero() {
		if flow.Service.ExpireAt == "" {
			flow.Err = nil
			flow.Msg = "missing payment expire at"

			MetricsPaymentsValidationError.Inc()

			return false
		} else {
			layout := "2006-01-02T15:04:05-07:00"
			expireAt, err := time.Parse(layout, flow.Service.ExpireAt)
			if err != nil {
				flow.Err = nil
				flow.Msg = "error parsing payment expire at from service"

				MetricsPaymentsValidationError.Inc()

				return false
			}
			flow.Result.Payment.ExpireAt.Time = expireAt
		}
	}
	if len(flow.Service.Split) > 0 {

	}

	return true
}

func (flow *FlowEventPaymentReceived) checkPrecondition() bool {
	if flow.Result.TenantID != flow.Service.TenantID {
		flow.Msg = "wrong tenant for this service"
		return false
	}
	if !flow.Tenant.Active {
		flow.Msg = "tenant not active"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	if !flow.Service.Active {
		flow.Msg = "service not active"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	if flow.Result.ID == "" {
		flow.Msg = "missing id"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	return true
}

func (flow *FlowEventPaymentReceived) loadStoredPayment() bool {
	paymentsCache := flow.Sctx.PaymentsCache()

	storedPayment, err := paymentsCache.Get(flow.Sctx.Ctx(), flow.Result.ID)
	flow.StoredPayment = storedPayment
	if err != nil && (err.Error() == "value not found in store" || err.Error() == "not found") {
		flow.Msg = "new payment found"

		MetricsPaymentsNew.Inc()

		return true
	}
	if err != nil && err.Error() != "value not found in store" && err.Error() != "not found" {
		flow.Msg = "get payment by id error for: " + flow.Result.ID

		MetricsPaymentsInternalError.Inc()

		return false
	}

	return true
}

func (flow *FlowEventPaymentReceived) checkPostcondition() bool {
	serverConfig := flow.Sctx.ServerConfig()

	if flow.StoredPayment == nil && flow.Result.Status != "CREATION_PENDING" {
		flow.Msg = "payment not in CREATION_PENDING status should be found on storage"

		MetricsPaymentsValidationError.Inc()

		return false
	}
	if flow.StoredPayment != nil && flow.Result.ID == flow.StoredPayment.ID {
		flow.Msg = "payment already handled"

		return false
	}
	if strings.HasPrefix(flow.Result.AppID, serverConfig.AppName) {
		flow.Msg = "payment event is from this proxy"
		return false
	}

	return true
}
