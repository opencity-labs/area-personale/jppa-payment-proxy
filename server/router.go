package server

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/swaggest/openapi-go/openapi3"
	"github.com/swaggest/rest/nethttp"
	"github.com/swaggest/rest/web"
	"github.com/swaggest/swgui/v5emb"
)

func registerRoutes(sctx *ServerContext, service *web.Service) {
	basePath := sctx.ServerConfig().BasePath

	setInfos(sctx, service)

	service.Get(basePath+"online-payment/{payment_id}", GetPaymentOnline(sctx), nethttp.SuccessStatus(http.StatusTemporaryRedirect))
	service.Get(basePath+"landing/{payment_id}", GetPaymentLanding(sctx), nethttp.SuccessStatus(http.StatusTemporaryRedirect))
	service.Get(basePath+"notice/{payment_id}", GetPaymentNotice(sctx), nethttp.SuccessfulResponseContentType("application/pdf"))
	service.Get(basePath+"receipt/{payment_id}", GetPaymentReceipt(sctx), nethttp.SuccessfulResponseContentType("application/pdf"))
	service.Get(basePath+"update/{payment_id}", GetPaymentUpdate(sctx), nethttp.SuccessStatus(http.StatusNoContent))

	service.Get(basePath+"tenants/schema", GetTenantSchema(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Options(basePath+"tenants/schema", OptionsTenantSchema(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Get(basePath+"tenants/{tenant_id}", GetTenantByID(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Put(basePath+"tenants/{tenant_id}", UpdateTenant(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Delete(basePath+"tenants/{tenant_id}", DisableTenant(sctx), nethttp.SuccessStatus(http.StatusNoContent))
	service.Patch(basePath+"tenants/{tenant_id}", PatchTenant(sctx), nethttp.SuccessStatus(http.StatusOK), nethttp.SuccessfulResponseContentType("application/json-patch+json"))
	service.Options(basePath+"tenants/{tenant_id}", OptionsTenantByID(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Post(basePath+"tenants", CreateTenant(sctx), nethttp.SuccessStatus(http.StatusCreated))
	service.Options(basePath+"tenants", OptionsCreateTenant(sctx), nethttp.SuccessStatus(http.StatusOK))

	service.Get(basePath+"configs/schema", GetServiceSchema(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Options(basePath+"configs/schema", OptionsServiceSchema(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Get(basePath+"configs/{service_id}", GetServiceByID(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Put(basePath+"configs/{service_id}", UpdateService(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Delete(basePath+"configs/{service_id}", DisableService(sctx), nethttp.SuccessStatus(http.StatusNoContent))
	service.Patch(basePath+"configs/{service_id}", PatchService(sctx), nethttp.SuccessStatus(http.StatusOK), nethttp.SuccessfulResponseContentType("application/json-patch+json"))
	service.Options(basePath+"configs/{service_id}", OptionsServiceByID(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Post(basePath+"configs", CreateService(sctx), nethttp.SuccessStatus(http.StatusCreated))
	service.Get(basePath+"configs", GetConfigs(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Options(basePath+"configs", OptionsCreateService(sctx), nethttp.SuccessStatus(http.StatusOK))

	service.Get(basePath+"status", GetStatus(sctx), nethttp.SuccessfulResponseContentType("application/problem+json"))

	service.Method(http.MethodGet, basePath+"metrics", promhttp.Handler())

	if basePath != "" {
		service.Method(http.MethodGet, "/metrics", promhttp.Handler())
	}

	service.Docs(basePath+"docs", v5emb.New)
}

func setInfos(sctx *ServerContext, service *web.Service) {
	appName := sctx.ServerConfig().AppName
	externalApiUrl := sctx.ServerConfig().ExternalApiUrl

	service.OpenAPISchema().SetTitle("JPPA Payment Proxy API")
	service.OpenAPISchema().SetDescription("Questo servizio si occupa di fare da intermediario tra l'area personale e il provider di pagamento " + appName)
	service.OpenAPISchema().SetVersion(VERSION)

	openapiSchema := service.OpenAPICollector.Reflector().Spec

	contactName := "Support"
	contactUrl := "https://www.opencitylabs.it"
	contactEmail := "support@opencitylabs.it"
	openapiSchema.Info.WithContact(openapi3.Contact{
		Name:  &contactName,
		URL:   &contactUrl,
		Email: &contactEmail,
	})
	openapiSchema.Info.WithTermsOfService("https://opencitylabs.it/")
	openapiSchema.Info.WithMapOfAnything(map[string]interface{}{
		"x-api-id":  appName,
		"x-summary": "Questo servizio si occupa di fare da intermediario tra l'area personale e il provider di pagamento " + appName,
	})
	serverDescription := appName
	openapiSchema.WithServers(openapi3.Server{
		URL:         externalApiUrl,
		Description: &serverDescription,
	})
	openapiSchema.WithTags([]openapi3.Tag{
		{
			Name: "Tenants",
		},
		{
			Name: "Services",
		},
		{
			Name: "Payments",
		},
		{
			Name: "Utilities",
		},
	}...)
}
