package server

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server/models"
)

type FlowProviderPaymentAuthenticate struct {
	Name      string
	Sctx      *ServerContext
	Tenant    *models.Tenant
	Client    *http.Client
	Request   *http.Request
	Err       error
	Msg       string
	Status    bool
	Result    *models.JppaPaymentAuthenticationResponse
	AuthToken string
}

func (flow *FlowProviderPaymentAuthenticate) Exec() bool {
	flow.Name = "ProviderPaymentAuthenticate"

	flow.Status = true &&
		flow.initRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderPaymentAuthenticate) initRequest() bool {
	requestData := models.JppaPaymentAuthenticationRequest{
		MsgID:    uuid.NewV4().String(),
		Code:     flow.Tenant.Code,
		Username: flow.Tenant.PaymentsUsername,
		Password: flow.Tenant.PaymentsPassword,
	}

	requestDataJson, err := json.Marshal(requestData)
	if err != nil {
		flow.Err = err
		flow.Msg = "json marshal error while preparing payment authentication request"
		return false
	}

	apiEndpoint := flow.Tenant.PaymentsEndpoint + "/login"
	request, err := http.NewRequest("POST", apiEndpoint, bytes.NewReader(requestDataJson))
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing payment authentication request"
		return false
	}

	request.Header.Add("Content-Type", "application/json")

	flow.Request = request

	return true
}

func (flow *FlowProviderPaymentAuthenticate) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_authenticate"))

	flow.Client = &http.Client{}

	response, err := flow.Client.Do(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting payment authentication request"

		MetricsPaymentsProviderError.WithLabelValues("payment_authenticate").Inc()

		return false
	}
	defer response.Body.Close()
	dueBody, err := io.ReadAll(response.Body)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while reading payment authentication response"

		MetricsPaymentsProviderError.WithLabelValues("payment_authenticate").Inc()

		return false
	}

	flow.Result = &models.JppaPaymentAuthenticationResponse{}
	err = json.Unmarshal(dueBody, flow.Result)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error while reading payment authentication response"

		MetricsPaymentsProviderError.WithLabelValues("payment_authenticate").Inc()

		return false
	}

	if flow.Result.Result != "OK" || flow.Result.Accesstoken == "" {
		flow.Err = err
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.AuthToken = flow.Result.Accesstoken

	return true
}
