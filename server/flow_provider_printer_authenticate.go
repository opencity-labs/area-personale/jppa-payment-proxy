package server

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server/models"
)

type FlowProviderPrinterAuthenticate struct {
	Name      string
	Sctx      *ServerContext
	Tenant    *models.Tenant
	Client    *http.Client
	Request   *http.Request
	Err       error
	Msg       string
	Status    bool
	Result    *models.JppaPrinterAuthenticationResponse
	AuthToken string
}

func (flow *FlowProviderPrinterAuthenticate) Exec() bool {
	flow.Name = "ProviderPrinterAuthenticate"

	flow.Status = true &&
		flow.initRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderPrinterAuthenticate) initRequest() bool {
	requestData := models.JppaPrinterAuthenticationRequest{
		Username: flow.Tenant.PrinterUsername,
		Password: flow.Tenant.PrinterPassword,
	}
	requestDataJson, err := json.Marshal(requestData)
	if err != nil {
		flow.Err = err
		flow.Msg = "json marshal error while preparing printer authentication request"
		return false
	}

	apiEndpoint := flow.Tenant.PrinterEndpoint + "/authenticate"
	request, err := http.NewRequest("POST", apiEndpoint, bytes.NewReader(requestDataJson))

	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing printer authentication request"
		return false
	}

	request.Header.Add("Content-Type", "application/json")

	flow.Request = request

	return true
}

func (flow *FlowProviderPrinterAuthenticate) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("printer_authenticate"))

	flow.Client = &http.Client{}

	response, err := flow.Client.Do(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting printer authentication request"

		MetricsPaymentsProviderError.WithLabelValues("printer_authenticate").Inc()

		return false
	}
	defer response.Body.Close()
	dueBody, err := io.ReadAll(response.Body)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while reading printer authentication response"

		MetricsPaymentsProviderError.WithLabelValues("printer_authenticate").Inc()

		return false
	}

	flow.Result = &models.JppaPrinterAuthenticationResponse{}
	err = json.Unmarshal(dueBody, flow.Result)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error while reading printer authentication response"

		MetricsPaymentsProviderError.WithLabelValues("printer_authenticate").Inc()

		return false
	}

	if flow.Result.Accesstoken == "" {
		flow.Err = err
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("create_printer_due").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.AuthToken = flow.Result.Accesstoken

	return true
}
