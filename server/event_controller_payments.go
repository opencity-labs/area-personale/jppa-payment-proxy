package server

import (
	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
)

func HandlePaymentsEventMessage(sctx *ServerContext, event *kafka.Message) {
	sctx.TenantsSync().RLock()
	defer sctx.TenantsSync().RUnlock()
	sctx.ServicesSync().RLock()
	defer sctx.ServicesSync().RUnlock()
	sctx.PaymentsSync().Lock()
	defer sctx.PaymentsSync().Unlock()

	FlowEventStoreExternalPayment := &FlowEventStoreExternalPayment{
		Sctx:  sctx,
		Event: event,
	}
	if !FlowEventStoreExternalPayment.Exec() {
		if FlowEventStoreExternalPayment.Err != nil {
			sctx.LogKafkaDebug().Str("event", string(event.Value)).Str("result", "discarded").
				Str("flow", FlowEventStoreExternalPayment.Name).
				Stack().Err(FlowEventStoreExternalPayment.Err).
				Msg(FlowEventStoreExternalPayment.Msg)
		} else {
			sctx.LogKafkaDebug().Str("event", string(event.Value)).Str("result", "discarded").
				Str("flow", FlowEventStoreExternalPayment.Name).
				Msg(FlowEventStoreExternalPayment.Msg)
		}
	}

	flowEventPaymentReceived := &FlowEventPaymentReceived{
		Sctx:  sctx,
		Event: event,
	}
	if !flowEventPaymentReceived.Exec() {
		if flowEventPaymentReceived.Err != nil {
			sctx.LogKafkaDebug().Str("event", string(event.Value)).Str("result", "discarded").
				Str("flow", flowEventPaymentReceived.Name).
				Stack().Err(flowEventPaymentReceived.Err).
				Msg(flowEventPaymentReceived.Msg)
		} else {
			sctx.LogKafkaDebug().Str("event", string(event.Value)).Str("result", "discarded").
				Str("flow", flowEventPaymentReceived.Name).
				Msg(flowEventPaymentReceived.Msg)
		}
		return
	}

	flowProviderPaymentAuthenticate := &FlowProviderPaymentAuthenticate{
		Sctx:   sctx,
		Tenant: flowEventPaymentReceived.Tenant,
	}
	if !flowProviderPaymentAuthenticate.Exec() {
		if flowProviderPaymentAuthenticate.Err != nil {
			sctx.LogKafkaDebug().Str("event", string(event.Value)).Str("result", "discarded").
				Str("flow", flowProviderPaymentAuthenticate.Name).Any("response", flowProviderPaymentAuthenticate.Result).
				Stack().Err(flowProviderPaymentAuthenticate.Err).
				Msg(flowProviderPaymentAuthenticate.Msg)
		} else {
			sctx.LogKafkaDebug().Str("event", string(event.Value)).Str("result", "discarded").
				Str("flow", flowProviderPaymentAuthenticate.Name).Any("response", flowProviderPaymentAuthenticate.Result).
				Msg(flowProviderPaymentAuthenticate.Msg)
		}
		return
	}

	flowProviderCreatePaymentDue := &FlowProviderCreatePaymentDue{
		Sctx:      sctx,
		Payment:   flowEventPaymentReceived.Result,
		Service:   flowEventPaymentReceived.Service,
		Tenant:    flowEventPaymentReceived.Tenant,
		AuthToken: flowProviderPaymentAuthenticate.AuthToken,
	}
	if !flowProviderCreatePaymentDue.Exec() {
		if flowProviderCreatePaymentDue.Err != nil {
			sctx.LogKafkaDebug().Str("event", string(event.Value)).Str("result", "discarded").
				Str("flow", flowProviderCreatePaymentDue.Name).Any("response", flowProviderCreatePaymentDue.Result).
				Stack().Err(flowProviderCreatePaymentDue.Err).
				Msg(flowProviderCreatePaymentDue.Msg)
		} else {
			sctx.LogKafkaDebug().Str("event", string(event.Value)).Str("result", "discarded").
				Str("flow", flowProviderCreatePaymentDue.Name).Any("response", flowProviderCreatePaymentDue.Result).
				Msg(flowProviderCreatePaymentDue.Msg)
		}
		// return
	}
	flowEventPaymentCreated := &FlowEventPaymentCreated{
		Sctx:         sctx,
		CreationFlow: flowProviderCreatePaymentDue,
	}
	if !flowEventPaymentCreated.Exec() {
		if flowEventPaymentCreated.Err != nil {
			sctx.LogKafkaDebug().Str("event", string(event.Value)).Str("result", "discarded").
				Str("flow", flowEventPaymentCreated.Name).Any("payment", flowEventPaymentCreated.Result).
				Stack().Err(flowEventPaymentCreated.Err).
				Msg(flowEventPaymentCreated.Msg)
		} else {
			sctx.LogKafkaDebug().Str("event", string(event.Value)).Str("result", "discarded").
				Str("flow", flowEventPaymentCreated.Name).Any("payment", flowEventPaymentCreated.Result).
				Msg(flowEventPaymentCreated.Msg)
		}
		return
	}
	sctx.LogKafkaInfo().Str("state", flowEventPaymentCreated.Result.Status).Any("payment", flowEventPaymentCreated.Result).Msg("event handled")
}
