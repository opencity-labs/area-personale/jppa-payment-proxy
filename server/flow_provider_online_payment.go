package server

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server/models"
)

type FlowProviderOnlinePayment struct {
	Name          string
	Sctx          *ServerContext
	Payment       *models.Payment
	Service       *models.Service
	Tenant        *models.Tenant
	AuthToken     string
	Request       *http.Request
	Client        *http.Client
	Err           error
	Msg           string
	Status        bool
	Result        *models.JppaOnlineResponse
	Url           string
	TransactionID string
}

func (flow *FlowProviderOnlinePayment) Exec() bool {
	flow.Name = "ProviderOnlinePayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderOnlinePayment) prepareRequest() bool {
	serverConfig := flow.Sctx.ServerConfig()
	referenceYear, err := strconv.Atoi(flow.Payment.CreatedAt.Format("2006"))
	if err != nil {
		flow.Err = err
		flow.Msg = "reference year conversion error while preparing online request"
		return false
	}
	var payerPostalCode int
	if flow.Payment.Payer.PostalCode != "" {
		payerPostalCode, err = strconv.Atoi(flow.Payment.Payer.PostalCode)
		if err != nil {
			flow.Err = err
			flow.Msg = "postal code conversion error while preparing online request"
			return false
		}
	}

	var payerType, payerCompanyName string
	if flow.Payment.Payer.Type == "human" {
		payerType = "F"
		payerCompanyName = ""
	} else {
		payerType = "G"
		payerCompanyName = flow.Payment.Payer.Name + " " + flow.Payment.Payer.FamilyName
	}

	dueSplits := []*models.JppaOnlineRequestSplit{}

	if flow.Payment.Payment.Split != nil && len(flow.Payment.Payment.Split) != 0 {
		for _, paymentSplit := range flow.Payment.Payment.Split {

			dueSplit := &models.JppaOnlineRequestSplit{
				Year:             paymentSplit.Meta.Year,
				EntryCode:        paymentSplit.Meta.EntryCode,
				EntryDescription: paymentSplit.Meta.EntryDescription,
				Amount:           *paymentSplit.Amount,
			}

			dueSplits = append(dueSplits, dueSplit)
		}
	} else {
		dueSplit := &models.JppaOnlineRequestSplit{
			Year:             int32(referenceYear),
			EntryCode:        flow.Service.EntryCode,
			EntryDescription: flow.Service.EntryDescription,
			Amount:           flow.Payment.Payment.Amount,
		}

		dueSplits = append(dueSplits, dueSplit)
	}

	onlineDataRequest := models.JppaOnlineRequest{
		TenantCode:  flow.Tenant.Code,
		ServiceCode: flow.Service.Code,
		Debits: []*models.JppaOnlineRequestDebit{
			{
				ReasonUpdate:    "ALTRO",
				Reason:          flow.Payment.Reason,
				TenantCode:      flow.Tenant.Code,
				RequesterCode:   flow.Tenant.Code,
				OnlineID:        flow.Payment.ID,
				ServiceCode:     flow.Service.Code,
				OnlineKind:      flow.Service.Kind,
				ActivedAt:       flow.Payment.CreatedAt.Format("2006-01-02T15:04:05.000Z"),
				CreatedAt:       flow.Payment.CreatedAt.Format("2006-01-02T15:04:05.000Z"),
				ExpireAt:        flow.Payment.Payment.ExpireAt.Format("2006-01-02T15:04:05.000Z"),
				PaymentExpireAt: flow.Payment.Payment.ExpireAt.Format("2006-01-02T15:04:05.000Z"),
				Splits:          dueSplits,
				Group:           "1",
				PaymentID:       flow.Payment.ID,
				Amount:          flow.Payment.Payment.Amount,
				Order:           1,
				Payer: &models.JppaOnlineRequestPayer{
					PayerType:                    payerType,
					PayerTaxIdentificationNumber: flow.Payment.Payer.TaxIdentificationNumber,
					PayerCompanyName:             payerCompanyName,
					PayerName:                    flow.Payment.Payer.Name,
					PayerFamilyName:              flow.Payment.Payer.FamilyName,
					PayerEmail:                   flow.Payment.Payer.Email,
					PayerStreetName:              flow.Payment.Payer.StreetName,
					PayerBuildingNumber:          flow.Payment.Payer.BuildingNumber,
					PayerPostalCode:              int32(payerPostalCode),
					PayerTownName:                flow.Payment.Payer.TownName,
					PayerCountrySubdivision:      flow.Payment.Payer.CountrySubdivision,
					PayerCountry:                 flow.Payment.Payer.Country,
				},
				Description: flow.Service.Description,
				PositionID:  flow.Payment.ID,
			},
		},
		PaymentID: flow.Payment.ID,
		Payers:    false,
		Payer: &models.JppaOnlineRequestPayer{
			PayerType:                    payerType,
			PayerTaxIdentificationNumber: flow.Payment.Payer.TaxIdentificationNumber,
			PayerCompanyName:             payerCompanyName,
			PayerName:                    flow.Payment.Payer.Name,
			PayerFamilyName:              flow.Payment.Payer.FamilyName,
			PayerEmail:                   flow.Payment.Payer.Email,
			PayerStreetName:              flow.Payment.Payer.StreetName,
			PayerBuildingNumber:          flow.Payment.Payer.BuildingNumber,
			PayerPostalCode:              int32(payerPostalCode),
			PayerTownName:                flow.Payment.Payer.TownName,
			PayerCountrySubdivision:      flow.Payment.Payer.CountrySubdivision,
			PayerCountry:                 flow.Payment.Payer.Country,
		},
		Links: &models.JppaOnlineRequestLinks{
			ReturnCancel: serverConfig.ExternalApiUrl + serverConfig.BasePath + "landing/" + flow.Payment.ID + "?status=cancel",
			ReturnKO:     serverConfig.ExternalApiUrl + serverConfig.BasePath + "landing/" + flow.Payment.ID + "?status=ko",
			ReturnNotify: serverConfig.ExternalApiUrl + serverConfig.BasePath + "landing/" + flow.Payment.ID + "?status=notify",
			ReturnOK:     serverConfig.ExternalApiUrl + serverConfig.BasePath + "landing/" + flow.Payment.ID + "?status=ok",
		},
	}

	onlineDataRequestJson, err := json.Marshal(onlineDataRequest)
	if err != nil {
		flow.Err = err
		flow.Msg = "json marshal error while preparing online request"
		return false
	}
	apiEndpoint := flow.Tenant.PaymentsEndpoint + "/debiti/v1/paga"
	onlineReq, err := http.NewRequest("POST", apiEndpoint, bytes.NewReader(onlineDataRequestJson))

	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing online request"
		return false
	}
	authorization := "Bearer " + flow.AuthToken
	onlineReq.Header.Add("Authorization", authorization)
	onlineReq.Header.Add("Content-Type", "application/json")

	flow.Request = onlineReq

	return true
}

func (flow *FlowProviderOnlinePayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_online"))

	flow.Client = &http.Client{}

	onlineRes, err := flow.Client.Do(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting online request"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}
	defer onlineRes.Body.Close()
	onlineBody, err := io.ReadAll(onlineRes.Body)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while reading online response"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}
	flow.Result = &models.JppaOnlineResponse{}
	err = json.Unmarshal(onlineBody, flow.Result)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error while reading online response"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}

	if flow.Result.Result == nil || flow.Result.Result.Result != "OK" {
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}

	if flow.Result.UrlRedirect == "" {
		flow.Msg = "provider did not give us a URL for payment"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}

	if flow.Result.TransactionID == "" {
		flow.Msg = "provider did not give us a transaction id"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}

	timer.ObserveDuration()

	if flow.Payment.Locale != "" {
		flow.Result.UrlRedirect = flow.Result.UrlRedirect + "&lang=" + flow.Payment.Locale
	}
	flow.Url = flow.Result.UrlRedirect
	flow.TransactionID = flow.Result.TransactionID

	return true
}
