package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server/models"
)

type PBClient struct {
	BaseURL  string
	Identity string
	Password string
	token    string
}

func (client *PBClient) getToken() error {
	authURL := client.BaseURL + "/collections/users/auth-with-password"
	data := models.PbAuthpayload{
		Username: client.Identity,
		Password: client.Password,
	}
	jsonData, err := json.Marshal(data)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", authURL, bytes.NewReader(jsonData))
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	clientHTTP := &http.Client{}

	resp, err := clientHTTP.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to get token: %s", resp.Status)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	var result models.PbTokenResponse
	if err := json.Unmarshal(body, &result); err != nil {
		return err
	}

	client.token = result.Token
	return nil
}

func (client *PBClient) GetTenantURL(tenantID string) (string, error) {
	err := client.getToken()
	if err != nil {
		return "", err
	}
	tenantsURL := client.BaseURL + "/collections/sdc_tenants/records"
	req, err := http.NewRequest("GET", tenantsURL, nil)
	if err != nil {
		return "", err
	}

	q := req.URL.Query()
	q.Add("filter", fmt.Sprintf("uuid='%s'", tenantID))
	req.URL.RawQuery = q.Encode()
	req.Header.Set("Authorization", "Bearer "+client.token)

	clientHTTP := &http.Client{}
	resp, err := clientHTTP.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("failed to get tenant data: %s", resp.Status)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	var result models.PbResponse
	if err := json.Unmarshal(body, &result); err != nil {
		return "", err
	}

	if len(result.Items) > 0 {
		firstItem := result.Items[0]

		return firstItem.TemporaryURL, nil
	}

	return "", fmt.Errorf("info for tenant %s not found", tenantID)
}
