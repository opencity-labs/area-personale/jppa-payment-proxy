package server

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"io"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server/models"
)

type FlowProviderNoticePayment struct {
	Name      string
	Sctx      *ServerContext
	Payment   *models.Payment
	Service   *models.Service
	Tenant    *models.Tenant
	AuthToken string
	Request   *http.Request
	Client    *http.Client
	Err       error
	Msg       string
	Status    bool
	Result    *models.JppaNoticeResponse
	Received  bool
	Notice    []byte
}

func (flow *FlowProviderNoticePayment) Exec() bool {
	flow.Name = "ProviderNoticePayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderNoticePayment) prepareRequest() bool {

	onlineDataRequest := models.JppaNoticeRequest{
		Printer: flow.Tenant.PrinterCode,
		Notice:  flow.Payment.Payment.NoticeCode,
		Debit: &models.JppaNoticeRequestDebit{
			Tenant:      flow.Tenant.Code,
			ServiceKind: flow.Service.Kind,
			PaymentID1:  flow.Payment.ID,
			PaymentID2:  flow.Payment.ID,
			ServiceCode: flow.Service.Code,
		},
	}

	onlineDataRequestJson, err := json.Marshal(onlineDataRequest)
	if err != nil {
		flow.Err = err
		flow.Msg = "json marshal error while preparing due request"
		return false
	}

	apiEndpoint := flow.Tenant.PrinterEndpoint + "/stampa-avvisatura"
	noticeReq, err := http.NewRequest("POST", apiEndpoint, bytes.NewReader(onlineDataRequestJson))

	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing notice request"
		return false
	}
	authorization := "Bearer " + flow.AuthToken
	noticeReq.Header.Add("Authorization", authorization)
	noticeReq.Header.Add("Content-Type", "application/json")

	flow.Request = noticeReq
	return true
}

func (flow *FlowProviderNoticePayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_notice"))
	flow.Client = &http.Client{}

	noticeRes, err := flow.Client.Do(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting notice request"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}
	defer noticeRes.Body.Close()
	noticeBody, err := io.ReadAll(noticeRes.Body)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while reading notice response"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	flow.Result = &models.JppaNoticeResponse{}
	err = json.Unmarshal(noticeBody, flow.Result)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error while reading notice response"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	if flow.Result.Result == "ERROR" {
		flow.Err = err
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}
	notice := flow.Result.Notice

	if notice == "" {
		flow.Msg = "provider response empty"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	timer.ObserveDuration()
	flow.Notice, err = base64.StdEncoding.DecodeString(notice)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while decoding notice"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}
	flow.Received = true

	return true
}
