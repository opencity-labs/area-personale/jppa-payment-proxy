package server

import (
	"encoding/json"
	"fmt"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server/models"
)

type FlowUserLogged struct {
	Name                   string
	Sctx                   *ServerContext
	Tenant                 *models.Tenant
	Err                    error
	Msg                    string
	Status                 bool
	User                   *models.User
	Event                  *kafka.Message
	ImportPaymentsResponse *models.ImportPaymentsResponse
}

func (flow *FlowUserLogged) Exec() bool {
	flow.Name = "FlowUserLogged"

	flow.Status = true &&
		flow.extractUser() &&
		flow.checkFeatureFlag() &&
		flow.checkTenant()
	return flow.Status
}

func (flow *FlowUserLogged) extractUser() bool {
	eventValue := flow.Event.Value

	flow.User = &models.User{}
	err := json.Unmarshal(eventValue, flow.User)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	if flow.User.EventVersion != 2 {
		flow.Err = err
		flow.Msg = "unsupported event version"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	if flow.User.TenantID == "" {
		flow.Err = err
		flow.Msg = "missing tenant_id"

		MetricsPaymentsValidationError.Inc()

		return false
	}
	if len(flow.User.TaxCode) != 16 {
		flow.Err = err
		flow.Msg = "irrilevant user"
		return false
	}

	return true
}

func (flow *FlowUserLogged) checkFeatureFlag() bool {
	tenantsCache := flow.Sctx.ImportAllDuesCache()

	tenants, err := tenantsCache.Get(flow.Sctx.Ctx(), "ImportAllDues")
	if err != nil {
		fmt.Println("errore ImportAllDues: " + err.Error())
		flow.Err = err
		flow.Msg = "unable to get feature flag config"

		MetricsPaymentsInternalError.Inc()

		return false
	}
	for _, tenant := range tenants.Tenants {
		if tenant.ID == flow.User.TenantID {

			return true
		}
	}
	flow.Err = err
	flow.Msg = "irrilevant tenant: not enabled in Feature Flag"
	return false

}

func (flow *FlowUserLogged) checkTenant() bool {
	tenantsCache := flow.Sctx.TenantsCache()
	tenant, err := tenantsCache.Get(flow.Sctx.Ctx(), flow.User.TenantID)
	flow.Tenant = tenant
	if err != nil && err.Error() == "value not found in store" {
		flow.Err = err
		flow.Msg = "irrilevant tenant"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get tenant by id error for: " + flow.User.TenantID

		MetricsPaymentsInternalError.Inc()

		return false
	}

	return true
}
