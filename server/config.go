package server

import (
	"context"
	"time"

	"github.com/sethvargo/go-envconfig"
)

type ServerConfig struct {
	// Proxy config
	AppName string `env:"APP_NAME,default=jppa-payment-proxy"`
	Debug   bool   `env:"DEBUG,default=true"`
	// Sentry config
	SentryEnabled bool   `env:"SENTRY_ENABLED,default=true"`
	SentryToken   string `env:"SENTRY_TOKEN,default=https://c2628ad808302a86c6408da8fc9a9905@o348837.ingest.sentry.io/4505681073668096"`
	// Kafka config
	KafkaBootstrapServers string `env:"KAFKA_BOOTSTRAP_SERVERS,default=localhost:9092"`
	KafkaGroupId          string `env:"KAFKA_GROUP_ID,default=jppa_payment_proxy"`
	KafkaPaymentsTopic    string `env:"KAFKA_PAYMENTS_TOPIC,default=payments"`
	KafkaUsersTopic       string `env:"KAFKA_USERS_TOPIC,default=users"`
	// HTTP config
	HttpBind       string `env:"HTTP_BIND,default=0.0.0.0"`
	HttpPort       string `env:"HTTP_PORT,default=40019"`
	ExternalApiUrl string `env:"EXTERNAL_API_URL,default=https://opencity.app.zoonect.dev"`
	InternalApiUrl string `env:"INTERNAL_API_URL,required"`
	BasePath       string `env:"BASEPATH,default=/payment-proxy/jppa/"`
	// Cache config
	CacheExpiration time.Duration `env:"CACHE_EXPIRATION,default=5m"`
	CacheEviction   time.Duration `env:"CACHE_EVICTION,default=10m"`
	// Storage config
	StorageType             string `env:"STORAGE_TYPE,default=s3"` // s3,local,azure
	StorageBucket           string `env:"STORAGE_BUCKET,default=payments"`
	StorageConfigsBasePath  string `env:"STORAGE_CONFIGS_BASE_PATH,default=sdc-payments/jppa/tenants/"`
	StoragePaymentsBasePath string `env:"STORAGE_EVENTS_BASE_PATH,default=sdc-payments/jppa/payments/"`
	StorageLocalPath        string `env:"STORAGE_LOCAL_PATH,default=/data/payments"`
	// S3 config
	StorageS3Key      string `env:"STORAGE_S3_KEY,default=02tzwnQ8qYBRFdn1zzI8XwEXAMPLE"`
	StorageS3Secret   string `env:"STORAGE_S3_SECRET,default=02tzwnWAIRCBJwA7HtHUnsEXAMPLEKEY"`
	StorageS3Region   string `env:"STORAGE_S3_REGION,default=local"`
	StorageS3Endpoint string `env:"STORAGE_S3_ENDPOINT,default=localhost:9000"`
	StorageS3Ssl      bool   `env:"STORAGE_S3_SSL,default=false"`
	// Azure config
	StorageAzureAccount string `env:"STORAGE_AZURE_ACCOUNT,default=02tzwnQ8qYBRFdn1zzI8XwEXAMPLE"`
	StorageAzureKey     string `env:"STORAGE_AZURE_KEY,default=02tzwnWAIRCBJwA7HtHUnsEXAMPLEKEY"`
	// redis config
	RedisClusterUrl string `env:"REDIS_CLUSTER_URL"`
	RedisTlsEnabled bool   `env:"REDIS_TLS_ENABLED,default=false"`
	RedisPrefixKey  string `env:"REDIS_PREFIX_KEY,required"`
	//Pocket base config
	PbApiUrl  string `env:"PB_API_URL,required"`
	PbAuthUsr string `env:"PB_AUTH_USER,required"`
	PbAuthPsw string `env:"PB_AUTH_PSW,required"`
}

func LoadConfig(ctx context.Context) (serverConfig ServerConfig, err error) {
	err = envconfig.Process(ctx, &serverConfig)
	return
}
