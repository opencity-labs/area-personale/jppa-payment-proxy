package server

import (
	"context"
	"errors"

	uuid "github.com/satori/go.uuid"
	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server/models"
)

type RemoteCollection struct {
	ID   string `json:"id"`
	Type string `json:"type"` // application, service, altro
}

// Receiver rappresenta il beneficiario del pagamento.
type Receiver struct {
	TaxIdentificationNumber string `json:"tax_identification_number"`
	Name                    string `json:"name"`
}

func GetServiceSchema(sctx *ServerContext) usecase.Interactor {
	uc := usecase.NewInteractor(func(_ context.Context, _ struct{}, output *models.Schema) error {
		*output = models.ServiceSchema

		return nil
	})

	uc.SetTitle("Get Service Form Schema")
	uc.SetDescription("...")
	uc.SetTags("Configs")
	// uc.SetExpectedErrors(status.Internal)

	return uc
}
func OptionsServiceSchema(sctx *ServerContext) usecase.Interactor {
	uc := usecase.NewInteractor(func(_ context.Context, _ struct{}, output *options) error {
		*output = options{
			Message: "Options request successful",
		}
		return nil
	})

	uc.SetTitle("Options Configs Form Schema")
	uc.SetDescription("...")
	uc.SetTags("Configs")
	// uc.SetExpectedErrors(status.Internal)

	return uc
}

type getServiceByIDInput struct {
	ServiceId string `path:"service_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

type getConfigsInput struct {
	ConfigIDs []string `query:"config_id" description:"List of configuration IDs (max 5)" example:"config1,config2"`
}

func GetConfigs(sctx *ServerContext) usecase.Interactor {
	configsCache := sctx.ServicesCache()
	configsSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input getConfigsInput, output *[]models.Service) error {
		if len(input.ConfigIDs) == 0 || len(input.ConfigIDs) > 5 {
			return status.InvalidArgument
		}

		configsSync.RLock()
		defer configsSync.RUnlock()

		var results []models.Service
		for _, id := range input.ConfigIDs {
			config, err := configsCache.Get(ctx, id)
			if err != nil && err.Error() == "value not found in store" {
				continue
			}
			if err != nil {
				sctx.LogHttpError().Stack().Err(err).Msg("get config by id error for: " + id)
				return status.Internal
			}
			results = append(results, *config)
		}

		*output = results
		return nil
	})

	uc.SetTitle("Get Payment Configurations List")
	uc.SetDescription("Retrieve up to 5 configuration entries by their IDs")
	uc.SetTags("Configs")
	uc.SetExpectedErrors(status.InvalidArgument, status.NotFound)

	return uc
}
func OptionsCreateService(sctx *ServerContext) usecase.Interactor {
	uc := usecase.NewInteractor(func(_ context.Context, input createServiceInput, output *options) error {
		*output = options{
			Message: "Options request successful",
		}
		return nil
	})

	uc.SetTitle("Options Get Config Configuration")
	uc.SetDescription("...")
	uc.SetTags("Configs")
	// uc.SetExpectedErrors(status.Internal)

	return uc
}

func GetServiceByID(sctx *ServerContext) usecase.Interactor {
	servicesCache := sctx.ServicesCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input getServiceByIDInput, output *models.Service) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		service, err := servicesCache.Get(ctx, input.ServiceId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get service by id error for: " + input.ServiceId)
			return status.Internal
		}

		*output = *service

		return nil
	})

	uc.SetTitle("Get Service Configuration")
	uc.SetDescription("...")
	uc.SetTags("Configs")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type createServiceInput struct {
	ID               string                            `json:"id" description:"..."`
	TenantID         string                            `json:"tenant_id" description:"..." required:"true"`
	Active           bool                              `json:"active" description:"..." required:"true"`
	Code             string                            `json:"code" description:"..." required:"true"`
	Kind             string                            `json:"kind" description:"..." required:"true"`
	Description      string                            `json:"description" description:"..." required:"true"`
	EntryCode        string                            `json:"entry_code" description:"..." required:"true"`
	EntryDescription string                            `json:"entry_description" description:"..." required:"true"`
	Splitted         bool                              `json:"splitted" description:"..." required:"true"`
	Split            []*createServiceInputServiceSplit `json:"split" description:"..." required:"true"`
	PaymentType      string                            `json:"payment_type" description:"..." required:"true"`
	RemoteCollection RemoteCollection                  `json:"remote_collection" description:"..." required:"true"`
	Amount           float64                           `json:"amount" description:"..." required:"true"`    // deve essere maggiore di 0
	Reason           string                            `json:"reason" description:"..." required:"true"`    // obbligatorio, massimo 255 caratteri
	ExpireAt         string                            `json:"expire_at" description:"..." required:"true"` // deve essere futura e valida
	Receiver         Receiver                          `json:"receiver" description:"..." required:"false"`
	CollectionData   string                            `json:"collection_data" description:"..." required:"false"` // tassonomia pagoPA dei pagamenti
}

type createServiceInputServiceSplit struct {
	Code             string  `json:"split_code" description:"..." required:"true"`
	Amount           float64 `json:"split_amount" description:"..." required:"true" format:"float"`
	EntryCode        string  `json:"split_entry_code" description:"..." required:"true"`
	EntryDescription string  `json:"split_entry_description" description:"..." required:"true"`
}

func CreateService(sctx *ServerContext) usecase.Interactor {
	servicesCache := sctx.ServicesCache()
	tenantsCache := sctx.TenantsCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input createServiceInput, output *models.Service) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		if input.ID != "" {
			_, err := servicesCache.Get(ctx, input.ID)

			if err == nil || err.Error() != "value not found in store" {
				return status.AlreadyExists
			}

		} else {
			input.ID = uuid.NewV4().String()
		}

		_, err := tenantsCache.Get(ctx, input.TenantID)
		if err != nil && err.Error() == "tenant id not found in store" {
			return status.Wrap(err, status.InvalidArgument)
		}

		_, err = uuid.FromString(input.ID)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}

		_, err = uuid.FromString(input.TenantID)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}

		output.ID = input.ID
		output.TenantID = input.TenantID
		output.Active = input.Active
		output.Code = input.Code
		output.Kind = input.Kind
		output.Description = input.Description
		output.EntryCode = input.EntryCode
		output.EntryDescription = input.EntryDescription
		output.Splitted = input.Splitted
		output.Split = []models.ServiceSplit{}
		if input.Split != nil {
			for _, split := range input.Split {
				outputSplit := models.ServiceSplit{
					Code:             split.Code,
					Amount:           split.Amount,
					EntryCode:        split.EntryCode,
					EntryDescription: split.EntryDescription,
				}
				output.Split = append(output.Split, outputSplit)
			}
		}
		if input.PaymentType == "pagopa" || input.PaymentType == "stamp" {
			output.PaymentType = input.PaymentType
		} else {
			return status.Wrap(errors.New("payment type can be pagopa or stamp"), status.InvalidArgument)
		}
		output.RemoteCollection.ID = input.RemoteCollection.ID
		output.RemoteCollection.Type = input.RemoteCollection.Type
		output.Amount = input.Amount
		output.ExpireAt = input.ExpireAt
		output.Receiver.Name = input.Receiver.Name
		output.Receiver.TaxIdentificationNumber = input.Receiver.TaxIdentificationNumber
		output.CollectionData = input.CollectionData

		err = StoreService(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.Wrap(err, status.InvalidArgument)
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Save Service Configuration")
	uc.SetDescription("...")
	uc.SetTags("Configs")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type updateServiceInput struct {
	ServiceId        string                            `path:"service_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ID               string                            `json:"id" description:"..." required:"true"`
	TenantID         string                            `json:"tenant_id" description:"..." required:"true"`
	Active           bool                              `json:"active" description:"..." required:"true"`
	Code             string                            `json:"code" description:"..." required:"true"`
	Kind             string                            `json:"kind" description:"..." required:"true"`
	Description      string                            `json:"description" description:"..." required:"true"`
	EntryCode        string                            `json:"entry_code" description:"..." required:"true"`
	EntryDescription string                            `json:"entry_description" description:"..." required:"true"`
	Splitted         bool                              `json:"splitted" description:"..." required:"true"`
	Split            []*updateServiceInputServiceSplit `json:"split" description:"..." required:"true"`
	PaymentType      string                            `json:"payment_type" description:"..." required:"true"`
	RemoteCollection RemoteCollection                  `json:"remote_collection" description:"..." required:"true"`
	Amount           float64                           `json:"amount" description:"..." required:"true"`    // deve essere maggiore di 0
	Reason           string                            `json:"reason" description:"..." required:"true"`    // obbligatorio, massimo 255 caratteri
	ExpireAt         string                            `json:"expire_at" description:"..." required:"true"` // deve essere futura e valida
	Receiver         Receiver                          `json:"receiver" description:"..." required:"false"`
	CollectionData   string                            `json:"collection_data" description:"..." required:"false"` // tassonomia pagoPA dei pagamenti
}

type updateServiceInputServiceSplit struct {
	Code             string  `json:"split_code" description:"..." required:"true"`
	Amount           float64 `json:"split_amount" description:"..." required:"true" format:"float"`
	EntryCode        string  `json:"split_entry_code" description:"..." required:"true"`
	EntryDescription string  `json:"split_entry_description" description:"..." required:"true"`
}

func UpdateService(sctx *ServerContext) usecase.Interactor {
	servicesCache := sctx.ServicesCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input updateServiceInput, output *models.Service) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		service, err := servicesCache.Get(ctx, input.ServiceId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get service by id error for: " + input.ServiceId)
			return status.Internal
		}

		if input.ID != service.ID || input.TenantID != service.TenantID {
			return status.Wrap(err, status.InvalidArgument)
		}

		output.ID = input.ID
		output.TenantID = input.TenantID
		output.Active = input.Active
		output.Code = input.Code
		output.Kind = input.Kind
		output.Description = input.Description
		output.EntryCode = input.EntryCode
		output.EntryDescription = input.EntryDescription
		output.Splitted = input.Splitted
		output.Split = []models.ServiceSplit{}
		if input.Split != nil {
			for _, split := range input.Split {
				outputSplit := models.ServiceSplit{
					Code:             split.Code,
					Amount:           split.Amount,
					EntryCode:        split.EntryCode,
					EntryDescription: split.EntryDescription,
				}
				output.Split = append(output.Split, outputSplit)
			}
		}

		output.RemoteCollection.ID = input.RemoteCollection.ID
		output.RemoteCollection.Type = input.RemoteCollection.Type
		output.Amount = input.Amount
		output.ExpireAt = input.ExpireAt
		output.Receiver.Name = input.Receiver.Name
		output.Receiver.TaxIdentificationNumber = input.Receiver.TaxIdentificationNumber
		output.CollectionData = input.CollectionData

		err = StoreService(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.Wrap(err, status.InvalidArgument)
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Update Service Configuration")
	uc.SetDescription("...")
	uc.SetTags("Configs")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type patchServiceInput struct {
	ServiceId        string                           `path:"service_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Active           *bool                            `json:"active" description:"..."`
	Code             *string                          `json:"code" description:"..."`
	Kind             *string                          `json:"kind" description:"..."`
	Description      *string                          `json:"description" description:"..."`
	EntryCode        *string                          `json:"entry_code" description:"..."`
	EntryDescription *string                          `json:"entry_description" description:"..."`
	Splitted         *bool                            `json:"splitted" description:"..."`
	Split            []*patchServiceInputServiceSplit `json:"split" description:"..."`
	PaymentType      *string                          `json:"payment_type" description:"..."`
	RemoteCollection *RemoteCollection                `json:"remote_collection" description:"..."`
	Amount           *float64                         `json:"amount" description:"..."`    // deve essere maggiore di 0
	Reason           *string                          `json:"reason" description:"..."`    // obbligatorio, massimo 255 caratteri
	ExpireAt         *string                          `json:"expire_at" description:"..."` // deve essere futura e valida
	Receiver         *Receiver                        `json:"receiver" description:"..."`
	CollectionData   *string                          `json:"collection_data" description:"..."` // tassonomia pagoPA dei pagamenti
}

type patchServiceInputServiceSplit struct {
	Code             string  `json:"split_code" description:"..."`
	Amount           float64 `json:"split_amount" description:"..." format:"float"`
	EntryCode        string  `json:"split_entry_code" description:"..."`
	EntryDescription string  `json:"split_entry_description" description:"..."`
}

func PatchService(sctx *ServerContext) usecase.Interactor {
	servicesCache := sctx.ServicesCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input patchServiceInput, output *models.Service) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		service, err := servicesCache.Get(ctx, input.ServiceId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get service by id error for: " + input.ServiceId)
			return status.Internal
		}

		output.ID = service.ID
		output.TenantID = service.TenantID

		if input.Active == nil {
			output.Active = service.Active
		} else {
			output.Active = *input.Active
		}
		if input.Code == nil {
			output.Code = service.Code
		} else {
			output.Code = *input.Code
		}
		if input.Kind == nil {
			output.Kind = service.Kind
		} else {
			output.Kind = *input.Kind
		}
		if input.Description == nil {
			output.Description = service.Description
		} else {
			output.Description = *input.Description
		}
		if input.EntryCode == nil {
			output.EntryCode = service.EntryCode
		} else {
			output.EntryCode = *input.EntryCode
		}
		if input.EntryDescription == nil {
			output.EntryDescription = service.EntryDescription
		} else {
			output.EntryDescription = *input.EntryDescription
		}
		if input.Splitted == nil {
			output.Splitted = service.Splitted
		} else {
			output.Splitted = *input.Splitted
		}
		if input.Split == nil {
			output.Split = service.Split
		} else {
			output.Split = []models.ServiceSplit{}
			if input.Split != nil {
				for _, split := range input.Split {
					outputSplit := models.ServiceSplit{
						Code:             split.Code,
						Amount:           split.Amount,
						EntryCode:        split.EntryCode,
						EntryDescription: split.EntryDescription,
					}
					output.Split = append(output.Split, outputSplit)
				}
			}
		}

		if input.PaymentType != nil {
			if *input.PaymentType == "pagopa" || *input.PaymentType == "stamp" {
				output.PaymentType = *input.PaymentType
			} else {
				return status.Wrap(err, status.InvalidArgument)
			}
		} else {
			output.PaymentType = service.PaymentType
		}

		if input.RemoteCollection != nil {
			if input.RemoteCollection.ID != "" {
				output.RemoteCollection.ID = input.RemoteCollection.ID
			} else {
				output.RemoteCollection.ID = service.RemoteCollection.ID
			}
			if input.RemoteCollection.Type != "" {
				output.RemoteCollection.Type = input.RemoteCollection.Type
			} else {
				output.RemoteCollection.Type = service.RemoteCollection.Type
			}
		} else {
			output.RemoteCollection = service.RemoteCollection
		}

		if input.Amount == nil {
			output.Amount = service.Amount
		} else {
			output.Amount = *input.Amount
		}
		if input.ExpireAt == nil {
			output.ExpireAt = service.ExpireAt
		} else {
			output.ExpireAt = *input.ExpireAt
		}

		if input.Receiver != nil {
			if input.Receiver.Name != "" {
				output.Receiver.Name = input.Receiver.Name
			} else {
				output.Receiver.Name = service.Receiver.Name
			}
			if input.Receiver.TaxIdentificationNumber != "" {
				output.Receiver.TaxIdentificationNumber = input.Receiver.TaxIdentificationNumber
			} else {
				output.Receiver.TaxIdentificationNumber = service.Receiver.TaxIdentificationNumber
			}
		} else {
			output.Receiver = service.Receiver
		}

		if input.CollectionData == nil {
			output.CollectionData = service.CollectionData
		} else {
			output.CollectionData = *input.CollectionData
		}
		err = StoreService(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.Wrap(err, status.InvalidArgument)
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Update Existing Service Configuration")
	uc.SetDescription("...")
	uc.SetTags("Configs")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}
func OptionsServiceByID(sctx *ServerContext) usecase.Interactor {
	uc := usecase.NewInteractor(func(_ context.Context, input getServiceByIDInput, output *options) error {
		*output = options{
			Message: "Options request successful",
		}
		return nil
	})

	uc.SetTitle("Options Get Config Configuration")
	uc.SetDescription("...")
	uc.SetTags("Configs")
	// uc.SetExpectedErrors(status.Internal)

	return uc
}

type disableServiceInput struct {
	ServiceId string `path:"service_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func DisableService(sctx *ServerContext) usecase.Interactor {
	servicesCache := sctx.ServicesCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input disableServiceInput, _ *struct{}) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		service, err := servicesCache.Get(ctx, input.ServiceId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get service by id error for: " + input.ServiceId)
			return status.Internal
		}

		service.Active = false

		err = StoreService(sctx, service)
		if err != nil && err.Error() == "invalid data" {
			return status.Wrap(err, status.InvalidArgument)
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Disable Service Configuration")
	uc.SetDescription("...")
	uc.SetTags("Configs")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}
