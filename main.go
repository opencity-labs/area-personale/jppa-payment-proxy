package main

import (
	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server"
)

func main() {
	serverContext := server.InitServerContext()

	serverContext.StartProcess("payments kafka server", serverContext.ServerConfig().KafkaPaymentsTopic, server.StartKafkaServer, server.HandlePaymentsEventMessage)
	serverContext.StartProcess("users kafka server", serverContext.ServerConfig().KafkaUsersTopic, server.StartKafkaServer, server.HandleUserEventMessage)

	serverContext.StartProcess("http server", "none", server.StartHttpServer, nil)

	serverContext.Daemonize()
}
