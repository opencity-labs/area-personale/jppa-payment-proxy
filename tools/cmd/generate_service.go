package cmd

import (
	"encoding/json"
	"fmt"

	uuid "github.com/satori/go.uuid"
	"github.com/spf13/cobra"
	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server/models"
)

var generateServiceCmd = &cobra.Command{
	Use:   "service",
	Short: "A brief description",
	Long:  `A longer description.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Generated service json:")
		fmt.Println()

		tenant_id, err := cmd.Flags().GetString("tenant_id")
		if err != nil {
			panic(err)
		}
		code, err := cmd.Flags().GetString("code")
		if err != nil {
			panic(err)
		}
		kind, err := cmd.Flags().GetString("kind")
		if err != nil {
			panic(err)
		}
		description, err := cmd.Flags().GetString("description")
		if err != nil {
			panic(err)
		}
		entry_code, err := cmd.Flags().GetString("entry_code")
		if err != nil {
			panic(err)
		}
		entry_description, err := cmd.Flags().GetString("entry_description")
		if err != nil {
			panic(err)
		}
		split, err := cmd.Flags().GetBool("split")
		if err != nil {
			panic(err)
		}

		serviceSplits := []models.ServiceSplit{}
		if split {
			serviceSplits = []models.ServiceSplit{
				{
					Code:             "c_1",
					Amount:           16.00,
					EntryCode:        "123 one",
					EntryDescription: "123/2024",
				},
				{
					Code:             "a_1",
					Amount:           0.50,
					EntryCode:        "123 bis",
					EntryDescription: "123/2024",
				},
			}
		}

		service := models.Service{
			ID:               uuid.NewV4().String(),
			TenantID:         tenant_id,
			Active:           true,
			Code:             code,
			Kind:             kind,
			Description:      description,
			EntryCode:        entry_code,
			EntryDescription: entry_description,
			Splitted:         split,
			Split:            serviceSplits,
		}

		bytes, err := json.MarshalIndent(service, "", "  ")
		if err != nil {
			panic(err)
		}

		fmt.Println(string(bytes))
		fmt.Println()
	},
}

func init() {
	generateCmd.AddCommand(generateServiceCmd)

	generateServiceCmd.Flags().StringP("tenant_id", "t", "", "ID del tenant")
	generateServiceCmd.MarkFlagRequired("tenant_id")
	generateServiceCmd.Flags().StringP("code", "c", "OPENCITY", "Codice servizio")
	generateServiceCmd.Flags().StringP("kind", "k", "VOTIVE", "Causale servizio")
	generateServiceCmd.Flags().StringP("description", "d", "Multa per eccesso di velocità", "Descrizione servizio")
	generateServiceCmd.Flags().StringP("entry_code", "f", "123", "Codice accertamento bilancio disattivato")
	generateServiceCmd.Flags().StringP("entry_description", "g", "123/2024", "Descrizione accertamento bilancio disattivato")
	generateServiceCmd.Flags().BoolP("split", "b", false, "Con bilancio")
}
