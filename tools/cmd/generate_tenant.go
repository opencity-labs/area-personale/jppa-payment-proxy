package cmd

import (
	"encoding/json"
	"fmt"

	uuid "github.com/satori/go.uuid"
	"github.com/spf13/cobra"
	"gitlab.com/opencontent/stanza-del-cittadino/jppa-payment-proxy/server/models"
)

var generateTenantCmd = &cobra.Command{
	Use:   "tenant",
	Short: "A brief description",
	Long:  `A longer description.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Generated tenant json:")
		fmt.Println()

		production, err := cmd.Flags().GetBool("production")
		if err != nil {
			panic(err)
		}
		code, err := cmd.Flags().GetString("code")
		if err != nil {
			panic(err)
		}
		name, err := cmd.Flags().GetString("name")
		if err != nil {
			panic(err)
		}
		tax_number, err := cmd.Flags().GetString("tax_number")
		if err != nil {
			panic(err)
		}
		payments_endpoint, err := cmd.Flags().GetString("payments_endpoint")
		if err != nil {
			panic(err)
		}
		payments_username, err := cmd.Flags().GetString("payments_username")
		if err != nil {
			panic(err)
		}
		payments_password, err := cmd.Flags().GetString("payments_password")
		if err != nil {
			panic(err)
		}
		printer_endpoint, err := cmd.Flags().GetString("printer_endpoint")
		if err != nil {
			panic(err)
		}
		printer_username, err := cmd.Flags().GetString("printer_username")
		if err != nil {
			panic(err)
		}
		printer_password, err := cmd.Flags().GetString("printer_password")
		if err != nil {
			panic(err)
		}
		printer_code, err := cmd.Flags().GetString("printer_code")
		if err != nil {
			panic(err)
		}

		tenant := models.Tenant{
			ID:                      uuid.NewV4().String(),
			Active:                  true,
			Production:              production,
			Code:                    code,
			Name:                    name,
			TaxIdentificationNumber: tax_number,
			PaymentsEndpoint:        payments_endpoint,
			PaymentsUsername:        payments_username,
			PaymentsPassword:        payments_password,
			PrinterEndpoint:         printer_endpoint,
			PrinterUsername:         printer_username,
			PrinterPassword:         printer_password,
			PrinterCode:             printer_code,
		}

		bytes, err := json.MarshalIndent(tenant, "", "  ")
		if err != nil {
			panic(err)
		}

		fmt.Println(string(bytes))
		fmt.Println()
	},
}

func init() {
	generateCmd.AddCommand(generateTenantCmd)

	generateTenantCmd.Flags().Bool("production", false, "Modalità production?")

	generateTenantCmd.Flags().StringP("code", "c", "c_b240", "Codice del tenant")
	generateTenantCmd.Flags().StringP("name", "n", "Comune di Empoli", "Nome del tenant")
	generateTenantCmd.Flags().StringP("tax_number", "t", "03482920158", "Codice fiscale del tenant")

	generateTenantCmd.Flags().StringP("payments_endpoint", "e", "https://pagopa-staging.maggioli.cloud/jcitygov-pagopa/api/rest", "Endpoint API del tenant")
	generateTenantCmd.Flags().StringP("payments_username", "u", "service-opencity-empoli", "Username API del tenant")
	generateTenantCmd.Flags().StringP("payments_password", "p", "@F25|Dmv7:e(R3zy", "Password API del tenant")

	generateTenantCmd.Flags().StringP("printer_endpoint", "w", "https://pagopa-staging.maggioli.cloud/jppaPrinterApi/rest/printer/v2", "Endpoint API PDF del tenant")
	generateTenantCmd.Flags().StringP("printer_username", "x", "service-opencity-empoli", "Username API PDF del tenant")
	generateTenantCmd.Flags().StringP("printer_password", "y", "PrntColl@vg7r0kntg9xv", "Password API PDF del tenant")
	generateTenantCmd.Flags().StringP("printer_code", "z", "JPPAColl", "Codice API PDF del tenant")
}
